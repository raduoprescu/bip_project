import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import crawler.Crawler;
import io.Property;
import types.Item;
import types.RSSSource;

public class CrawlerTest {

//	@Test
//	public void testTheXMLFileIsRetrieved() {
//		assertNotNull(new Crawler().fetchFileContent("bbcNewsXMLfeed.xml"));
//	}

	@Test
	public void testCrawlerReturnsItems() {
		RSSSource testRSSsource = new RSSSource("BBC News", "Barcelona", "whatever", 1000);

		Property prop = new Property("test.config.properties", "test.config.passwords");
		ArrayList<Item> itemList = new Crawler().crawl(testRSSsource, prop);
		assertEquals("Paradise Papers: Tax haven secrets of ultra-rich exposed", itemList.get(0).getTitle());
		assertEquals("http://www.bbc.co.uk/news/uk-41876942", itemList.get(0).getLink());
		assertEquals(
				"A huge new leak reveals how the wealthy and powerful, including the Queen's private estate, invest offshore.",
				itemList.get(0).getDescription());
	}
	
	@Test
	public void testCrawlerDoesntCrawlTwoTimesSameSource() throws InterruptedException {
		RSSSource testRSSsource = new RSSSource("BBC News", "Barcelona", "whatever", 1000);

		Property prop = new Property("test.config.properties", "test.config.passwords");
		assertTrue(testRSSsource.readyToCrawlAgain());
		assertEquals(-1, testRSSsource.secondsFromLastScan());
		
		new Crawler().crawl(testRSSsource, prop);
		assertFalse(testRSSsource.readyToCrawlAgain());
		
		long s1 = testRSSsource.secondsFromLastScan();
		assertEquals(s1,0);
		Thread.sleep(2000);
		long s2 = testRSSsource.secondsFromLastScan();
		assertEquals(s2-s1, 2);
		
		new Crawler().crawl(testRSSsource, prop);
		assertFalse(testRSSsource.readyToCrawlAgain());
		
		s1 = testRSSsource.secondsFromLastScan();
		assertEquals(s1,0);
		
	}

}