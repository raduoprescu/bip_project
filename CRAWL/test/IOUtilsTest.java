import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import io.IOUtils;
import io.Property;
import types.RSSSource;

public class IOUtilsTest {

	@Test
	public void testScannedCSVRetrievesRSSSource() {
		Map<String, RSSSource> mapSources = new HashMap<String, RSSSource>();
		Property prop = new Property("test.config.properties", "test.config.passwords");
		IOUtils.scanForNewSources(mapSources, prop.getInputFileName());

		System.out.println(mapSources.keySet());
		assertEquals(3, mapSources.size());

		assertEquals("http://feeds.bbci.co.uk/news/rss.xml?edition=uk", mapSources.get("BBCNewsUK").getLink());
		assertEquals("UK", mapSources.get("BBCNewsUK").getLocation());
		assertEquals(5, mapSources.get("BBCNewsUK").getFrequency());
		assertEquals("BBCNewsUK", mapSources.get("BBCNewsUK").getNewsSource());
		
		assertEquals("http://www.zf.ro/rss/", mapSources.get("ZF").getLink());
		assertEquals("Romania", mapSources.get("ZF").getLocation());
		assertEquals(4, mapSources.get("ZF").getFrequency());
		assertEquals("ZF", mapSources.get("ZF").getNewsSource());
		
		assertEquals("http://romanialibera.ro/rss/actualitate/international-44.xml", mapSources.get("RomaniaLibera").getLink());
		assertEquals("Romania", mapSources.get("RomaniaLibera").getLocation());
		assertEquals(5, mapSources.get("RomaniaLibera").getFrequency());
		assertEquals("RomaniaLibera", mapSources.get("RomaniaLibera").getNewsSource());
		
	}

}
