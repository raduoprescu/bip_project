package types;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class RSSSource {
	private String newsSource;
	private String location;
	private String link;
	private int frequency; // number of times per day
	private LocalDateTime lastScanTime = null;
	private final int SECONDS_IN_A_DAY = 86400;
	private Date lastArticleSaved = null;


	public RSSSource(String newsSource, String location, String link, int frequency) {
		this.newsSource = newsSource;
		this.location = location;
		this.link = link.toLowerCase();
		this.frequency = frequency;
	}

	public boolean readyToCrawlAgain() {
		// if it hasn't been crawled ever then ready to scan 
		if (lastScanTime == null) {
			System.out.println("Source never crawled ==> Will Crawl \n\n");
			return true;
		}
		// if the time between scans is bigger then the number of minutes between scans then it's ready to crawl again 
		if (secondsFromLastScan() > (SECONDS_IN_A_DAY/this.frequency)) {
			System.out.println("Time between scans is big ==> Will crawl RSS\n\n");
			return true;
		}
		System.out.println("Time between scans is small ==> Will NOT Crawl RSS again\n\n");
		return false;
	}
	
	public long secondsFromLastScan() {
		if (lastScanTime == null)
			return -1;
		return ChronoUnit.SECONDS.between(lastScanTime,LocalDateTime.now());
	}

	public void scanned() {
		lastScanTime = LocalDateTime.now();
	}

	public boolean isSame(RSSSource a) {
		return !newsSource.equals(a.getNewsSource()) || !location.equals(a.getLocation()) || !link.equals(a.getLink())
				|| frequency != a.getFrequency();
	}

	public void update(RSSSource a) {
		newsSource = a.newsSource;
		location = a.location;
		link = a.link;
		frequency = a.frequency;
	}

	public String getNewsSource() {
		return newsSource;
	}

	public void setNewsSource(String newsSource) {
		this.newsSource = newsSource;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}
	
	public Date getLastArticleSaved() {
		return lastArticleSaved;
	}

	public void setLastArticleSaved(Date d) {
		lastArticleSaved = d;
	}
	
}
