package types;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Item {
	private String title = "";
	private String link = "";
	private String description = "";
	private RSSSource rssSource = null;
	private ArrayList<String> categories = new ArrayList<String>();
	private Date pubDate = null;
	private String creator = "";
	private String contentEncoded = "";
	private String content;
	
	public Item (RSSSource rs, String title, String link, String description) {
		this.rssSource = rs;
		this.title = title;
		this.link = link; 
		this.description = description;
	}
	
	public RSSSource getRSSSource() {
		return rssSource;
	}

	public void setRSSSource(RSSSource rs) {
		this.rssSource = rs;
	}

	public String getCategories() {
		return categories.toString();
	}

	public void setCategories(ArrayList<String> categories) {
		this.categories = categories;
	}
	
	public Date getPubDate() {
		return pubDate;
	}

	public String getPubDateAsString() {
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String currentTimeSQL = sdf.format(pubDate);
		return currentTimeSQL;
	}

	public void setPubDate(String pubDate) {
		DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
		try {
			Date date = formatter.parse(pubDate);
			this.pubDate = date;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getContentEncoded() {
		return contentEncoded;
	}

	public void setContentEncoded(String contentEncoded) {
		this.contentEncoded = contentEncoded;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	} 
	
	public String toString(){
		return "===============================" + 
			   "\n  title: \t\t" + this.title + 
			   "\n\n  link: \t\t" + this.link + 
			   "\n  description: \t\t" + this.description + 
			   "\n  pubDate: \t\t" + this.pubDate + 
			   "\n  creator: \t\t" + this.creator +
			   "\n  categories: \t\t" + this.categories.toString() +
			   "\n  content: \t\t" + this.contentEncoded +
			   "\n  RSS Source: \t\t" + this.rssSource.getNewsSource() + " " + this.rssSource.getLocation() + 
			   "\n===============================" +
			   "\n";
	}
	
	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

}