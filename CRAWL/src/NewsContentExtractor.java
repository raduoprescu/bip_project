import io.WebHelper;

public class NewsContentExtractor {
	public static void main(String[] args) throws Exception {
		// link as argument eg: https://politica.elpais.com/politica/2017/11/08/actualidad/1510142626_254684.html
		System.out.println(WebHelper.getNewsContent(args[0]));
	}
}