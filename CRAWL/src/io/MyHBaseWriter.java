package io;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.BufferedMutator;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;

import types.Item;

public class MyHBaseWriter implements MyWriter {
	
	private Configuration config;
	private Connection connection;
	
	private String key = "";
	
	protected HashMap<String,String> data;
	
	protected BufferedMutator buffer;
	
	public MyHBaseWriter() {
		this.reset();
	}

	public void open(String tableName) throws IOException {
		this.config = HBaseConfiguration.create();
		config.set("hadoop.security.authentication", "simple");
		config.set("hadoop.security.authorization","false");
		config.set("hbase.security.authentication", "simple");
		config.set("hbase.security.authorization","false");

		this.connection = ConnectionFactory.createConnection(this.config);
		this.buffer = this.connection.getBufferedMutator(TableName.valueOf(tableName));		
	}
	
	protected String nextKey() {
		return key;
	}
	
	protected String toFamily(String attribute) {
		return (attribute.equals("content") || attribute.equals("description") ) ? "family1" : "family2";
	}

	public void put(Item item) {
		key = item.getPubDateAsString() + "|" + item.getLink(); 
		data.put("content", item.getContent());
		data.put("description", item.getDescription());
		
		data.put("title", item.getTitle());
		data.put("categories", item.getCategories());
		data.put("location", item.getRSSSource().getLocation());
		data.put("sourceName", item.getRSSSource().getNewsSource());
		data.put("sourceURL", item.getRSSSource().getLink());
	}

	public void reset() {
		data = new HashMap<String, String>();
	}

	public int flush() throws IOException {
		String rowKey = this.nextKey();
		System.out.println("Row with key "+rowKey+" outputted");
	
		// Create a new Put object with an incremental key
		Put put = new Put(rowKey.getBytes());
		
		// Now get all the columns
		Set<Entry<String,String>> entries = this.data.entrySet();
		int length = 0;
		for (Entry<String, String> entry : entries) {
			// Add the value in the Put object
			String attribute = entry.getKey();
			String family = this.toFamily(attribute);
			String value = entry.getValue();
			put.addColumn(family.getBytes(), attribute.getBytes(), value.getBytes());
		
			length += value.length();
		}
		// Insert it!
		this.buffer.mutate(put);
		
		this.reset();
		return length;
	}

	public void close() throws IOException {
		this.buffer.flush();
		this.buffer.close();
		this.connection.close();
	}
}
