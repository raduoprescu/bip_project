package io;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import types.Item;

public class SQLServerDBHelper {
	private String url;
	private String user;
	private String password;
	private boolean saveToDb;

	public SQLServerDBHelper(Property prop) {
		url = prop.getDbURL();
		user = prop.getDbUserName();
		password = prop.getDbPassword();
		saveToDb = prop.getSaveToDb();
	}

	public void addNewsItem(Item a) {
		if (saveToDb) {
			try {
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
				Connection conn = DriverManager.getConnection(url, user, password);
				Statement st = conn.createStatement();

				String insertStatement = "INSERT INTO dbo.newsitems " + "VALUES ('" + a.getTitle().replaceAll("'", "")
						+ "', '" + a.getLink() + "', '" + a.getDescription().replaceAll("'", "") + "', '"
						+ a.getPubDateAsString() + "', '" + a.getCreator().replaceAll("'", "") + "', '"
						+ a.getCategories().toString().replaceAll("'", "") + "', '"
						+ a.getContentEncoded().replaceAll("'", "") + "', '"
						+ a.getRSSSource().getNewsSource().replaceAll("'", "") + "', '"
						+ a.getRSSSource().getLocation().replaceAll("'", "") + "')";

				st.executeUpdate(insertStatement);
				conn.close();
			} catch (Exception e) {
				System.err.println("Got an exception! ");
				System.err.println(e.getMessage());
			}
		}
	}

}
