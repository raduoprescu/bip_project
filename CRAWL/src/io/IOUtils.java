package io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import types.RSSSource;

public class IOUtils {

	public static void scanForNewSources(Map<String, RSSSource> existingSources, String fileName) {
		ArrayList<String[]> rssSourcesList = getRSSSourcesFromCSV(fileName);
		// remove header from ArrayList
		//String[] rssSourcesListHeader = rssSourcesList.get(0);
		rssSourcesList.remove(0);

		for (String[] rssSourceArray : rssSourcesList) {
			RSSSource rs = new RSSSource(rssSourceArray[0], rssSourceArray[1], rssSourceArray[2],
					Integer.parseInt(rssSourceArray[3]));
			if (!existingSources.containsKey(rssSourceArray[0])) {
				existingSources.put(rssSourceArray[0], rs);
			} else {
				if (!existingSources.get(rssSourceArray[0]).isSame(rs)) {
					existingSources.get(rssSourceArray[0]).update(rs);
				}
			}
		}
	}

	public static ArrayList<String[]> getRSSSourcesFromCSV(String fileName) {
		// it should have a header
		return CSVReader.getStringArrayCsvFile(fileName);
	}

	static class CSVReader {

		static ArrayList<String[]> getStringArrayCsvFile(String csvFile) {

			String line = "";
			String cvsSplitBy = ",";
			ArrayList<String[]> linesCSV = new ArrayList<String[]>();

			try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {

				while ((line = br.readLine()) != null) {

					// use comma as separator
					String[] splittedLine = line.split(cvsSplitBy);
					linesCSV.add(splittedLine);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

			return linesCSV;
		}

	}
}
