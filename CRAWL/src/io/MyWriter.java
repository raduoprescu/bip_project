package io;

import java.io.IOException;

public interface MyWriter {
	
	public void open(String file) throws IOException;
	
	public void reset();
	
	public int flush() throws IOException;
	
	public void close() throws IOException;
	
}
