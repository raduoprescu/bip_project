package io;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Property {
	private Properties prop = new Properties();
	private Properties pass = new Properties();
	private InputStream input_properties = null;
	private InputStream input_passwords = null;

	public Property(String configProperties, String configPasswords) {
		try {

			// load a properties file
			input_properties = getClass().getClassLoader().getResourceAsStream(configProperties);
			prop.load(input_properties);
			
			// load a passwords file
			input_passwords = getClass().getClassLoader().getResourceAsStream(configPasswords);
			pass.load(input_passwords);

		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input_properties != null) {
				try {
					input_properties.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public String getInputFileName () {
		return prop.getProperty("inputFileName");
	}
	
	public String getDbUserName () {
		return prop.getProperty("dbUserName");
	}
	
	public String getDbPassword () {
		return pass.getProperty("dbUserPass");
	}

	public String getDbURL() {
		return prop.getProperty("dbURL");
	}
	
	public boolean getDummyData() {
		return Boolean.valueOf(prop.getProperty("dummyData"));
	}
	
	public String getDummyDataSource() {
		return prop.getProperty("dummyDataSource");
	}
	
	public String getHBaseTableName() {
		return prop.getProperty("hbaseTableName");
	}

	public boolean getSaveToDb() {
		return Boolean.valueOf(prop.getProperty("saveToDb"));
	}
}
