
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import crawler.Crawler;
import io.IOUtils;
import io.MyHBaseWriter;
import io.Property;
import io.WebHelper;
import types.Item;
import types.RSSSource;

public class RSSExtractor {
	private static String CONFIG_PROPERTIES = "config.properties";
	private static String CONFIG_PASSWORDS = "config.passwords";

	public static void main(String[] args) throws Exception {
		Map<String, RSSSource> rssSources = new HashMap<String, RSSSource>();
		Map<String, Date> sourceLastCrawled = new HashMap<String, Date>();
		ArrayList<Item> newsItems = new ArrayList<Item>();
		Property prop = new Property(CONFIG_PROPERTIES, CONFIG_PASSWORDS);
		// SQLServerDBHelper dbHelp = new SQLServerDBHelper(prop);
		Crawler crawler = new Crawler();
		
		File file = new File("sources.state");  
		if (file.exists()) {
			FileInputStream f = new FileInputStream(file);  
			ObjectInputStream s = new ObjectInputStream(f);  
			sourceLastCrawled = (Map<String, Date>)s.readObject();         
			s.close();
			f.close();
		}

		// this needs to be put on a thread
		IOUtils.scanForNewSources(rssSources, prop.getInputFileName());
		
		// retrieve the last date
		for (RSSSource rssSource : rssSources.values()) {
			if (!sourceLastCrawled.containsKey(rssSource.getNewsSource()) ) {
				sourceLastCrawled.put(rssSource.getNewsSource(), null);
			}
			Date lastDate = sourceLastCrawled.get(rssSource.getNewsSource());
			rssSource.setLastArticleSaved(lastDate);
		}

		MyHBaseWriter hbWriter = new MyHBaseWriter();

		// now we go and scan
		for (;;) {
			System.out.println("Sleeping for 1s before trying to get the sources again\n\n");
			Thread.sleep(1000);

			for (RSSSource rs : rssSources.values()) {
				hbWriter.open(prop.getHBaseTableName());
				if (rs.readyToCrawlAgain()) {
					newsItems = crawler.crawl(rs, prop);
					Date lastDateForSource = null;
					for (Item newsItem : newsItems) {

						// Save to SQL Server
						// dbHelp.addNewsItem(newsItem);
						System.out.println(newsItem);

						// Fetch news content
						if (newsItem.getRSSSource().getLastArticleSaved() == null
								|| newsItem.getPubDate().after(newsItem.getRSSSource().getLastArticleSaved())) {
							System.out.println("Crawling the content for the item \n\n");
							newsItem.setContent(prop.getDummyData() ? "contentdummy" : WebHelper.getNewsContent(newsItem.getLink()));

							lastDateForSource = (lastDateForSource == null ? newsItem.getPubDate() : lastDateForSource);
							lastDateForSource = (newsItem.getPubDate().after(lastDateForSource) ? newsItem.getPubDate()
									: lastDateForSource);
						} else {
							System.out.println(
									"Not Crawling the content of the next items, it's before the last crawled date.\n\n"
											+ "news item date: " + newsItem.getPubDate() + " sourceLastDate: "
											+ newsItem.getRSSSource().getLastArticleSaved());
							break;
						}

						// Add the new record to hbase.
						hbWriter.put(newsItem);
						hbWriter.flush();

						hbWriter.reset();

						// Sleep for x second (between 2 -> 7 seconds)
						int sleepTime = ThreadLocalRandom.current().nextInt(2000, 7000);
						System.out.println("Sleeping for " + sleepTime / 1000 + " second between crawls \n\n\n");
						Thread.sleep(sleepTime);
					}

					System.out.println(
							"Saving the last date for source:" + rs.getNewsSource() + " date: " + lastDateForSource);
					rs.setLastArticleSaved(lastDateForSource);
					
					sourceLastCrawled.put(rs.getNewsSource(), lastDateForSource);
					
					// save the state of the sources
					file = new File("sources.state");
					FileOutputStream f = new FileOutputStream(file);
					ObjectOutputStream s = new ObjectOutputStream(f);
					s.writeObject(sourceLastCrawled);
					s.close();
					f.close();

				}
				hbWriter.close();
			}
		}

	}
}
