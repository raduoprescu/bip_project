package crawler;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import io.Property;
import types.Item;
import types.RSSSource;

public class Crawler {
	public ArrayList<Item> crawl(RSSSource rssSource, Property prop) {
		ArrayList<Item> itemsList = new ArrayList<Item>();

		// get document from link
		Document doc = getDocumentFromLink(rssSource.getLink(), prop);

		NodeList nodes = doc.getElementsByTagName("channel");
		Element element = (Element) nodes.item(0);
		NodeList items = element.getElementsByTagName("item");

		for (int i = 0; i < items.getLength(); i++) {
			Element item = (Element) items.item(i);
			NodeList title = item.getElementsByTagName("title");
			NodeList description = item.getElementsByTagName("description");
			NodeList link = item.getElementsByTagName("link");

			if (title.getLength() < 0 || link.getLength() < 0 || description.getLength() < 0 ||
					title.item(0) == null || description.item(0) == null || description.item(0) == null || 
					title.item(0).getFirstChild() == null || description.item(0).getFirstChild() == null || description.item(0).getFirstChild() == null )
				continue;
			// Create News Item with compulsory fields
			Item newsItem = new Item(rssSource, title.item(0).getFirstChild().getTextContent(),
					link.item(0).getFirstChild().getTextContent(),
					description.item(0).getFirstChild().getTextContent());

			// Try to add the other fields
			NodeList pubDate = item.getElementsByTagName("pubDate");
			NodeList creator = item.getElementsByTagName("dc:creator");
			NodeList contentEncoded = item.getElementsByTagName("content:encoded");

			ArrayList<String> categoriesList = new ArrayList<String>();
			NodeList categories = item.getElementsByTagName("category");
			if (categories != null) {
				for (int j = 0; j < categories.getLength(); j++) {
					// if (categories.item(j) != null)
					// if (categories.item(j).getFirstChild() != null)
					categoriesList.add(categories.item(j).getFirstChild().getTextContent());
				}
			}
			newsItem.setPubDate(pubDate == null || pubDate.item(0) == null ? "" : pubDate.item(0).getFirstChild().getTextContent());
			//newsItem.setCreator(creator == null || creator.item(0) == null ? "" : creator.item(0).getFirstChild().getTextContent());
			newsItem.setContentEncoded(contentEncoded == null || contentEncoded.item(0) == null ? "" : contentEncoded.item(0).getFirstChild().getTextContent());
			newsItem.setCategories(categoriesList);

			itemsList.add(newsItem);

		}

		// RSS Source Scanned -- update last scan time.
		rssSource.scanned();

		return itemsList;
	}

	protected Document getDocumentFromLink(String link, Property prop) {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = null;
			if (prop.getDummyData()) {
				String fileName = prop.getDummyDataSource();
				doc = dBuilder.parse(new File(fileName));
				System.out.println("Using dummy data... ");
			} else {
				System.out.println("Using real data... " + link);
				doc = dBuilder.parse(link);
			}
			return doc;
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	// public String fetchFileContent(String fileName) {
	//
	// StringBuilder fileContents = new StringBuilder((int) file.length());
	// Scanner scanner;
	// try {
	// scanner = new Scanner(file);
	// String lineSeparator = System.getProperty("line.separator");
	//
	// while (scanner.hasNextLine()) {
	// fileContents.append(scanner.nextLine() + lineSeparator);
	// }
	// scanner.close();
	// return fileContents.toString();
	// } catch (FileNotFoundException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// return null;
	// }

}
