# DATA PREPARATION - the first step of the analysis
# Removing stop words(English),  stemming,
# Install "NLTK" library (Linux) see JIRA

from nltk.corpus import stopwords
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from collections import Counter
import string
from nltk import FreqDist
from nltk.stem.lancaster import LancasterStemmer
from nltk.stem.snowball import SnowballStemmer



# Replace the text example with a news article,  string format.

example_text = "The BBC was given permission to report from the Republic of Uzbekistan in Central Asia" \
               " for the first time in over 12 years. Since the death of the authoritarian President Islam Karimov" \
               " last year, a cautious programme of reform has been carried out in the secretive nation." \
               "We asked ordinary Uzbeks how they feel about the changes taking place in their country."

# REMOVING STOP WORDS and PUNCTUATION
# we are using english text, in case the text is in other languages the dictionary should be changed
punctuation = list(string.punctuation)
stop_words = set(stopwords.words("english") + punctuation)
words = word_tokenize(example_text)
StopWordsRemoved = []
StopWordsRemoved = [w for w in words if not w in stop_words]
print(set(StopWordsRemoved)) ### JUST for sprint MTG
# SET stop words removed

#STEMMING WITH PYTHON
from nltk.stem import PorterStemmer
ps = PorterStemmer()
StemWords = [[ps.stem(token) for token in sentence.split(" ")] for sentence in StopWordsRemoved]
print(StemWords)

# tf - idf

wordSet = StemWords
wordDictA = dict.fromkeys(wordSet, 0)
for word in wordSet:
    wordDictA[word] += 1

import pandas as pd

pd.DataFrame([wordDictA])


def computeTF(wordDict, bow):
    tfDict = {}
    bowCount = len(bow)
    for word, count in wordDict.iteritems():
        tfDict[word] = count / float(bowCount)
    return tfDict


def computeIDF(docList):
    import math
    idfDict = {}
    N = len(docList)

