# DATA PREPARATION - the first step of the analysis
# Removing stop words(English),  stemming,
# Install "NLTK" library (Windows, Linux) README file

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.tag import pos_tag

# Replace the text example with a news article,  string format.
# tf-id

example_text = "The referendum was approved by the Catalan parliament in a session " \
               " along with a law which states that independence would be binding with a simple majority," \
               " without requiring a minimum turnout USA. I will add Putin and Trump to check the proper names " \
               "extracion. After being suspended, the law was finally declared Obama " \
               "void on 17 October and is also illegal according to the Catalan Statutes of Autonomy which " \
               "require a two Obama third majority in the Catalan Putin parliament for any change to Catalonia's status"

# EXTRACT PROPER NOUNS
# NNP might not be the most frequent word, but they are important for the search
tokens = word_tokenize(example_text)
tagged = pos_tag(tokens)
nouns = [word for word, pos in tagged if (pos == 'NNP')]
unique_proper_words = set(nouns)
# The list of proper nouns
print(unique_proper_words)