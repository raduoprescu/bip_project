import string
import nltk
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD

example_text = "The BBC was given permission to report from the Republic of Uzbekistan in Central Asia" \
               " for the first time in over 12 years. Since the death of the authoritarian President Islam Karimov" \
               " last year, a cautious programme of reform has been carried out in the secretive nation." \
               "We asked ordinary Uzbeks how they feel about the changes taking place in their country."

# REMOVING STOP WORDS and PUNCTUATION
# we are using english text, in case the text is in other languages the dictionary should be changed
punctuation = list(string.punctuation)
stop_words = set(stopwords.words("english") + punctuation)
words = word_tokenize(example_text)
StopWordsRemoved = []
StopWordsRemoved = [w for w in words if not w in stop_words]
print(set(StopWordsRemoved)) ### JUST for sprint MTG

#postDocs[0]
vectorizer = TfidfVectorizer(StopWordsRemoved, use_idf=True, ngram_range=(1,3))
x = vectorizer.fit_transform(StopWordsRemoved)
print(vectorizer)
lsa = TruncatedSVD(n_components=27, n_iter=100)
lsa.fit(x)
lsa.components_[0]
terms = vectorizer.get_feature_names()
for i, comp in enumerate(lsa.components_):
 termsInComp = zip(terms,comp)
 #sortedTerms = sorted(termsInComp, key-lambda x: x[1])



import rake
import operator
rake_object = rake.Rake("SmartStoplist.txt", 5, 3, 4)



keywords = rake_object.run(example_text)
print("Keywords:", keywords)
