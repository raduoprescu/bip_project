<?php

use Illuminate\Database\Seeder;
use FeedMeNews\User;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        $user = User::get();
//        foreach ($user as $rd) {
//            $rd->delete();
//        }
//        dd($user->count());
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 10; $i++) {
            User::create([
                'firstName' => $faker->firstName(),
                'lastName' => $faker->lastName,
                'email' => $faker->email,
                'encodedPassword' => Hash::make($faker->password),
                'created_at' => $faker->dateTime,
                'updated_at' => $faker->dateTime
            ]);
        }
    }
}
