<?php

use Illuminate\Database\Seeder;
use FeedMeNews\Articles;

class ArticlesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
//dd($faker->date('Y-m-d H:i:s'));

        for ($i = 0; $i < 100000; $i++) {
            $articles = Articles::create(
                [
                    'name' => $faker->randomElement(['BBC News', 'El Pais', 'CNN', 'Euro News',
                        'El Mundo', 'New York Times']),
                    'location' => $faker->randomElement(['Barcelona', 'Madrid', 'Paris', 'London',
                        'Prague', 'Rome', 'Amsterdam', 'Berlin', 'Brussels', 'Lisbon']),
                    'category' => $faker->randomElement(['Science', 'Travel', 'Politics', 'Entertainment',
                        'Technology', 'Business', 'Culture', 'Food', 'Fashion']),
                    'keywords' => $faker->randomElements(['Science', 'Travel', 'Politics', 'Entertainment',
                        'Technology', 'Business', 'Culture', 'Food', 'Fashion'], rand(1, 9)),
                    'sentiment' => $faker->randomElement(['Positive', 'Negative', 'Neutral']),
                    'cos' => $faker->randomFloat(),
                    'url' => $faker->url,
                    'date' => $faker->date('Y-m-d H:i:s'),
                    'description' => $faker->realText(rand(100, 300)),
                ]
            );
        }
    }
}
