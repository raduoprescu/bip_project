<?php

namespace FeedMeNews;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use DB;

class Articles extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'articles';
    protected $guarded = [];

    protected $casts = [
//        'created_at',
//        'updated_at',
        'publishDate'
    ];

    public static function getGroupBySource($name)
    {
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) use ($name) {
            $test_result = $collection->aggregate(
                [
                    ['$sort' => ['publishDate' => -1]],
//                    ['$limit' => 10],
                    ['$group' => ['_id' => '$source.' . $name, 'news' => ['$push' => '$$ROOT']]],
                ]
            );
//dd('wer are ');
            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
//dd($results);
        return $results;
    }

    public static function getSentiment()
    {
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) {
            $test_result = $collection->aggregate(
                [
                    ['$sort' => ['publishDate' => -1]],
//                    ['$limit' => 10],
                    ['$group' => ['_id' => '$sentiment.value', 'news' => ['$push' => '$$ROOT']]],
                ]
            );
            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
//dd($results);
        return $results;
    }

    //                    ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
    public static function getAnalysisGroup($type, $sources)
    {
        //Count positive, negative, neutral
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) use ($type, $sources) {
            if ($sources == '') {
                $test_result = $collection->aggregate(
                    [
//                    ['$match' => ['source.name' => ['$in' => $sources]]],
                        ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],
                        ['$sort' => ['count' => 1]],
                        ['$group' => ['_id' => '$_id.name', 'values' => ['$push' => ['sentiment' => '$_id.sentiment', 'count' => '$count']]]],

                    ]
                );
            } else {
                $test_result = $collection->aggregate(
                    [
                        ['$match' => ['source.name' => ['$in' => $sources]]],
                        ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],
                        ['$sort' => ['count' => 1]],
                        ['$group' => ['_id' => '$_id.name', 'values' => ['$push' => ['sentiment' => '$_id.sentiment', 'count' => '$count']]]],

                    ]
                );
            }

            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
        return $results;
    }

    public static function getAnalysisText($type, $text, $start_date, $end_date, $sources = null)
    {
        //Count positive, negative, neutral
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) use ($type, $text, $start_date, $end_date, $sources) {
//            dd($sources);
            if ($text == '' && $sources == null) {
                $test_result = $collection->aggregate(
                    [
//                    ['$project' => ['' => -1]],
//                    ['$match' => ['$text' => ['$search' => $text]]],
//                        ['$match' => ['keywords' => $text]],
                        ['$match' => ['publishDate' => ['$gte' => $start_date, '$lte' => $end_date]]],
                        ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],
                        ['$sort' => ['count' => 1]],
                        ['$group' => ['_id' => '$_id.name', 'values' => ['$push' => ['sentiment' => '$_id.sentiment', 'count' => '$count']]]],

                    ]
                );
            } else if ($text == '') {
                $test_result = $collection->aggregate(
                    [
//                    ['$project' => ['' => -1]],
//                    ['$match' => ['$text' => ['$search' => $text]]],
//                        ['$match' => ['keywords' => $text]],
                        ['$match' => ['source.name' => ['$in' => $sources]]],
                        ['$match' => ['publishDate' => ['$gte' => $start_date, '$lte' => $end_date]]],
                        ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],
                        ['$sort' => ['count' => 1]],
                        ['$group' => ['_id' => '$_id.name', 'values' => ['$push' => ['sentiment' => '$_id.sentiment', 'count' => '$count']]]],

                    ]
                );
            } else {
//                dd($sources);
                if ($sources == '') {
                    $test_result = $collection->aggregate(
                        [
//                    ['$project' => ['' => -1]],
//                    ['$match' => ['$text' => ['$search' => $text]]],
//                        ['$unwind' => '$keywords'],
//                            ['$match' => ['source.name' => ['$in' => $sources]]],
                            ['$match' => ['keywords' => $text]],
                            ['$match' => ['publishDate' => ['$gte' => $start_date, '$lte' => $end_date]]],
                            ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],
                            ['$sort' => ['count' => 1]],
                            ['$group' => ['_id' => '$_id.name', 'values' => ['$push' => ['sentiment' => '$_id.sentiment', 'count' => '$count']]]],

                        ]
                    );
                } else {
//                    dd($type);
                    $test_result = $collection->aggregate(
                        [
//                    ['$project' => ['' => -1]],
//                    ['$match' => ['$text' => ['$search' => $text]]],
//                        ['$unwind' => '$keywords'],
                            ['$match' => ['source.name' => ['$in' => $sources]]],
                            ['$match' => ['keywords' => $text]],
                            ['$match' => ['publishDate' => ['$gte' => $start_date, '$lte' => $end_date]]],
                            ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],
                            ['$sort' => ['count' => 1]],
                            ['$group' => ['_id' => '$_id.name', 'values' => ['$push' => ['sentiment' => '$_id.sentiment', 'count' => '$count']]]],

                        ]
                    );
                }
            }

            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
//dd($results);
        return $results;
    }


    public static function getGroupByCount($name)
    {
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) use ($name) {
            $test_result = $collection->aggregate(
                [
                    ['$sort' => ['publishDate' => -1]],
//                    ['$limit' => 10],
                    ['$group' => ['_id' => '$source.' . $name, 'count' => ['$sum' => 1]]],
//                    ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],

                ]
            );
//dd('wer are ');
            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });
//dd($results);
        return $results;
    }

    public static function getTrendingKeywords($start_date, $end_date, $text, $sources = null, $sentiment = null)
    {
//        dd($text);
        $queryBuilder = DB::connection('mongodb')->collection('articles');
        $results = $queryBuilder->raw(function ($collection) use ($start_date, $end_date, $text, $sources, $sentiment) {
            $test_result = '';
//            dd($sources);
            if (($text == '' || $text == null) && ($sources == '' || $sources[0] == '' || $sources == null) && $sentiment == null) {
//                dd($start_date);
                $test_result = $collection->aggregate(
                    [
                        ['$unwind' => '$keywords'],
                        ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
                        ['$group' => ['_id' => '$keywords', 'count' => ['$sum' => 1]]],
                        ['$sort' => ['count' => -1]],
                    ]
                );
            } else {

                if (($text == '' || $text == null) && ($sources == '' || $sources[0] == '' || $sources == null)) {
                    $test_result = $collection->aggregate(
                        [
                            ['$match' => ['sentiment.value' => $sentiment]],
                            ['$unwind' => '$keywords'],
                            ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
                            ['$group' => ['_id' => '$keywords', 'count' => ['$sum' => 1]]],
                            ['$sort' => ['count' => -1]],
                        ]
                    );
                } else if (($sources == '' || $sources[0] == '' || $sources == null) && $sentiment == null) {
//                    dd('h');
                    $test_result = $collection->aggregate(
                        [
                            ['$match' => ['keywords' => $text]],
//                            ['$match' => ['sentiment.value' => $sentiment]],
                            ['$unwind' => '$keywords'],
                            ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
                            ['$group' => ['_id' => '$keywords', 'count' => ['$sum' => 1]]],
                            ['$sort' => ['count' => -1]],
                        ]
                    );
                } else if ($sources == '' || $sources[0] == '' || $sources == null) {
//                    dd('h');
                    $test_result = $collection->aggregate(
                        [
                            ['$match' => ['keywords' => $text]],
                            ['$match' => ['sentiment.value' => $sentiment]],
                            ['$unwind' => '$keywords'],
                            ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
                            ['$group' => ['_id' => '$keywords', 'count' => ['$sum' => 1]]],
                            ['$sort' => ['count' => -1]],
                        ]
                    );
                } else if ($text == '' || $text == null) {
//                    dd('this');
                    $test_result = $collection->aggregate(
                        [
                            ['$match' => ['sentiment.value' => $sentiment]],
                            ['$match' => ['source.name' => ['$in' => $sources]]],
                            ['$unwind' => '$keywords'],
                            ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
                            ['$group' => ['_id' => '$keywords', 'count' => ['$sum' => 1]]],
                            ['$sort' => ['count' => -1]],
                        ]
                    );
                } else if ($sentiment == null) {
//                    dd('this');
                    $test_result = $collection->aggregate(
                        [
                            ['$match' => ['keywords' => $text]],
                            ['$match' => ['source.name' => ['$in' => $sources]]],
                            ['$unwind' => '$keywords'],
                            ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
                            ['$group' => ['_id' => '$keywords', 'count' => ['$sum' => 1]]],
                            ['$sort' => ['count' => -1]],
                        ]
                    );
                } else {
//                    dd($sources[0]);
                    $test_result = $collection->aggregate(
                        [
                            ['$match' => ['sentiment.value' => $sentiment]],
                            ['$match' => ['source.name' => ['$in' => $sources]]],
                            ['$match' => ['keywords' => $text]],
                            ['$unwind' => '$keywords'],
                            ['$match' => ['publishDate' => ['$gte' => $start_date, '$lt' => $end_date]]],
                            ['$group' => ['_id' => '$keywords', 'count' => ['$sum' => 1]]],
                            ['$sort' => ['count' => -1]],
                        ]
                    );
                }

            }


            $array = iterator_to_array($test_result);
            $json = \MongoDB\BSON\toJSON(\MongoDB\BSON\fromPHP($array));
            return json_decode($json, true);
        });

        return $results;
    }
}



//                    ['$project' => ['keywords' => ['$split' => ['$keywords', " "]], 'publishDate' => 1]],
//                    ['$unwind' => '$keywords'],
//                    ['$sort' => ['publishDate' => -1]],
//                    ['$unwind' => '$keywords'],
//                    ['$limit' => 10],
//                    ['$group' => ['_id' => ['name' => '$' . $type, 'sentiment' => '$sentiment.value'], 'count' => ['$sum' => 1]]],
//                    ['$split' => ['$keywords', ', ']],