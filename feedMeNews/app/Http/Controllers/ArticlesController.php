<?php

namespace FeedMeNews\Http\Controllers;

use Carbon\Carbon;
use FeedMeNews\Articles;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd(asset('images/logos/fox-news.jpg'));
//        dd($request->search);
        $count = Articles::count();
        $take_articles = 10;
        $data['sources'] = Articles::distinct('source.name')->get()->toArray();

        if ($request->search == null && $request->search_city == null && $request->daterange == null) {

            $data['articles'] = Articles::orderBy('publishDate', 'DESC')->paginate(10);
            $data['current_city'] = '';
            $data['current_search'] = '';
            $data['current_source'] = '';
            $data['selected_sources'] = [];

            $date = new \DateTime();
            $date->setDate(2017, 01, 1);
            $data['start_date'] = $date->format('m/d/Y');
            $data['current_date'] = date('m/d/Y');
            $data['keywords'] = Articles::getTrendingKeywords(Carbon::now()->subDays(30)->toIso8601String(), Carbon::now()->toIso8601String(), null);
            $data['keywords'] = array_slice($data['keywords'], 0, 10);

            $data['sentiment'] = '';

//            dd($data['keywords']);
//            dd($data['articles']->links());
        } else {
//            dd($request->all());
            $date_range = $request->daterange;
            $date_range = explode("-", $date_range);
            $date_part_1 = new Carbon($date_range[0]);
            $date_part_2 = new Carbon($date_range[1]);
            $search_sources = explode(',', $request->search_source);
//            dd($search_sources);
//            dd($date_part_1->eq($date_part_2));
            if ($date_part_1->eq($date_part_2)) {
                $date_part_2 = $date_part_2->addDay();
            }
//            dd($date_part_1->toDateTimeString().' - '.$date_part_2->toDateTimeString());
            $rand = rand(0, $count);
            $rand_side = rand(0, $count);

            if ($request->search_city != null && $request->search != null && $request->search_source != null && $request->sentiment_group != null) {
                $starttime = microtime(true);
                $data['articles'] = Articles::where('keywords', 'like', '%' . $request->search . '%')
                    ->whereIn('source.name', $search_sources)
                    ->where('sentiment.value', '=', $request->sentiment_group)
                    ->where('source.location', '=', $request->search_city)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $data['sources'] = Articles::where('source.location', '=', $request->search_city)
                    ->distinct('source.name')->get()->toArray();
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
//                dd($duration);
            } else if ($request->search_city != null && $request->search != null && $request->search_source != null) {
                $starttime = microtime(true);
                $data['articles'] = Articles::where('keywords', 'like', '%' . $request->search . '%')
                    ->whereIn('source.name', $search_sources)
                    ->where('source.location', '=', $request->search_city)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $data['sources'] = Articles::where('source.location', '=', $request->search_city)
                    ->distinct('source.name')->get()->toArray();
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
//                dd($duration);
            } else if ($request->search_source != null && $request->search_city != null && $request->sentiment_group != null) {
//                dd('ere');
                $starttime = microtime(true);
                $data['articles'] = Articles::whereIn('source.name', $search_sources)
                    ->where('sentiment.value', '=', $request->sentiment_group)
                    ->where('source.location', '=', $request->search_city)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
            } else if ($request->search != null && $request->search_city != null && $request->sentiment_group != null) {
//                dd('ere');
                $starttime = microtime(true);
                $data['articles'] = Articles::where('keywords', '=', $request->search)
                    ->where('sentiment.value', '=', $request->sentiment_group)
                    ->where('source.location', '=', $request->search_city)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
            } else if ($request->search != null && $request->search_source != null && $request->sentiment_group != null) {
//                dd('ere');
                $starttime = microtime(true);
                $data['articles'] = Articles::where('keywords', '=', $request->search)
                    ->where('sentiment.value', '=', $request->sentiment_group)
                    ->whereIn('source.name', $search_sources)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
            } elseif ($request->search_city != null && $request->search_source != null) {
                $starttime = microtime(true);
                $data['articles'] = Articles::where('source.name', '=', $request->search_source)
                    ->where('source.location', '=', $request->search_city)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $data['sources'] = Articles::where('source.location', '=', $request->search_city)
                    ->distinct('source.name')->get()->toArray();
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
//                dd($duration);
            } elseif ($request->search_source != null && $request->search != null) {
                $starttime = microtime(true);
                $data['articles'] = Articles::where('keywords', 'like', '%' . $request->search . '%')
                    ->whereIn('source.name', $search_sources)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
//                dd($duration);
            } elseif ($request->search_city != null && $request->search != null) {
                $starttime = microtime(true);
                $data['articles'] = Articles::where('keywords', 'like', '%' . $request->search . '%')
                    ->where('source.location', '=', $request->search_city)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $data['sources'] = Articles::where('source.location', '=', $request->search_city)
                    ->distinct('source.name')->get()->toArray();
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
//                dd($duration);
            } else if ($request->search_city != null && $request->sentiment_group != null) {
                $starttime = microtime(true);
                $data['articles'] = Articles::where('sentiment.value', '=', $request->sentiment_group)
                    ->where('source.location', '=', $request->search_city)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
            } else if ($request->search_source != null && $request->sentiment_group != null) {
                $starttime = microtime(true);
                $data['articles'] = Articles::where('sentiment.value', '=', $request->sentiment_group)
                    ->whereIn('source.name', $search_sources)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
            } else if ($request->search != null && $request->sentiment_group != null) {
                $starttime = microtime(true);
//                dd('kjsdh/');
                //greater than or equal, bug here when we search with between dates for chart and articles
                $data['articles'] = Articles::where('keywords', '=', $request->search)
                    ->where('sentiment.value', '=', $request->sentiment_group)
//                    ->where('publishDate', '<=', $date_part_2->format('Y-m-d H:i:s'))
//                    ->where('publishDate', '>=', $date_part_1->format('Y-m-d H:i:s'))
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
            } else if ($request->search_city != null) {
                $starttime = microtime(true);
                $data['articles'] = Articles::where('source.location', '=', $request->search_city)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $data['sources'] = Articles::where('source.location', '=', $request->search_city)
                    ->distinct('source.name')->get()->toArray();
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
//                dd($duration);
//                dd($data['articles']);
            } else if ($request->search != null) {

                $starttime = microtime(true);

                $data['articles'] = Articles::where('keywords', 'like', '%' . $request->search . '%')
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
//                $executionTime = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
//                dd($duration);
//dd($data['articles']);
            } else if ($request->search_source != null) {
//dd('he');
                $starttime = microtime(true);

                $data['articles'] = Articles::whereIn('source.name', $search_sources)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
//                $executionTime = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
//                dd($duration);
//dd($data['articles']);
            } else if ($request->sentiment_group != null) {
                $starttime = microtime(true);
                $data['articles'] = Articles::where('sentiment.value', '=', $request->sentiment_group)
                    ->whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
//                $executionTime = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
//                dd($duration);

//                dd($data['articles']);
            } else if ($request->daterange != null) {
                $starttime = microtime(true);
                $data['articles'] = Articles::whereBetween('publishDate', [$date_part_1->toDateTimeString(), $date_part_2->toDateTimeString()])
                    ->orderBy('publishDate', 'DESC')->paginate(10);
                $endtime = microtime(true);
                $duration = $endtime - $starttime;
//                $executionTime = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
//                dd($duration);

//                dd($data['articles']);
            }
//dd($request->search_source);
            if ($request->search_source == null) {
                $data['selected_sources'] = [];
            } else {
                $data['selected_sources'] = $search_sources;
            }
            $data['current_search'] = $request->search;
            $data['current_city'] = $request->search_city;
            $data['current_source'] = $request->search_source;

            $data['start_date'] = $date_range[0];
            $data['current_date'] = $date_range[1];
            $data['keywords'] = Articles::getTrendingKeywords($date_part_1->toIso8601String(), $date_part_2->toIso8601String(), $request->search, $search_sources, $request->sentiment_group);
            $data['keywords'] = array_slice($data['keywords'], 0, 10);

            $data['sentiment'] = $request->sentiment_group;

        }
//        dd($data['keywords']);
        $data['date_range'] = $request->daterange;;
        $data['take_articles'] = $take_articles;
        $data['cities'] = Articles::distinct('source.location')->get()->toArray();


//        return view('index', $data);//
//        return view('front.index', $data);//
        return view('front.new', $data);//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $data['articles'] = Articles::where('source.name', 'like', '%' . $request->search . '%')
//            ->orWhere('source.location', '=', $request->search_city)
////                ->orWhereIn('keyword', [$request->search])
//            ->orWhere('keywords', 'like', '%' . $request->search . '%')
//            ->whereBetween('publishDate', [$date_part_1->toDateString(), $date_part_2->toDateString()])
////                ->whereBetween('date', [$date_part_1->format('Y-m-d H:i:s'), $date_part_2->format('Y-m-d H:i:s')])
////                ->where('date', '>=', $date_part_1->format('Y-m-d H:i:s'))
////                ->where('date', '<=', $date_part_2->format('Y-m-d H:i:s'))
//            ->orderBy('publishDate', 'DESC')->get();
////                ->orderBy('publishDate', 'DESC')->get();
////                ->take(20)->paginate(8);
        //                ->orWhere('title', 'like', '%' . $request->search . '%')
//                ->orWhere('description', 'like', '%' . $request->search . '%')
//                ->orWhere('category', 'like', '%' . $request->search . '%')
//                ->where('date', '>=', $date_part_1->toDateString())
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd('submitted');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
