<?php

namespace FeedMeNews\Http\Controllers;

use Dingo\Api\Facade\Route;
use Illuminate\Http\Request;
use FeedMeNews\Articles;
use Carbon\Carbon;

class AnalysisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request_type = '';
//        dd($request->type);
        $request->type = str_replace("By ", "", $request->type);
        if ($request->type == '' || $request->type == 'Sources') {
            $request_type = 'source.name';
        } else if ($request->type == 'Location') {
            $request_type = 'source.location';
        } else if ($request->type == 'Sentiment') {
            $request_type = 'sentiment.value';
        }
        if ($request->type == '') {
            $data['type_group'] = '';
        } else {
            $data['type_group'] = 'By ' . $request->type;
        }

        $data['type_group_chart'] = $request->type;

        if ($request->search == null && $request->daterange == null) {
//            $data['sources'] = Articles::distinct('source.name')->get()->toArray();

//            $data['sources'] = array_collapse($data['sources']);
//        dd($articles);

            $articles = Articles::getAnalysisGroup($request_type, '');
//            dd($articles);
            $sources = [];
            $results = Array();
            foreach ($articles as $rd) {
//            $sources[]=
                $sources[$rd['_id']] = [
                    'source' => $rd['_id'],
                    isset($rd['values'][0]['sentiment']) ? $rd['values'][0]['sentiment'] : '' =>
                        isset($rd['values'][0]['sentiment']) ? $rd['values'][0]['count'] : '',
                    isset($rd['values'][1]['sentiment']) ? $rd['values'][1]['sentiment'] : '' =>
                        isset($rd['values'][1]['sentiment']) ? $rd['values'][1]['count'] : '',
                    isset($rd['values'][2]['sentiment']) ? $rd['values'][2]['sentiment'] : '' =>
                        isset($rd['values'][2]['sentiment']) ? $rd['values'][2]['count'] : '',
                ];
            }
            //sources filter here with dropdown
//            dd($sources);
//            $sources = array_slice($sources, 0, 12);
            $results = [];
            foreach ($sources as $rd) {
                $negative = isset($rd['positive']) ? $rd['positive'] : 0;
                $positive = isset($rd['negative']) ? $rd['negative'] : 0;
                $neutral = isset($rd['neutral']) ? $rd['neutral'] : 0;
                $sum = $negative + $positive + $neutral;
                $results[] = [isset($rd['source']) ? $rd['source'] : '', isset($rd['positive']) ? $rd['positive'] : 0
                    , isset($rd['negative']) ? $rd['negative'] : 0, isset($rd['neutral']) ? $rd['neutral'] : 0, $sum];
            }
            usort($results, function ($b, $a) {
                return $a['4'] - $b['4'];
            });
            array_unshift($results, ['Source', 'Positive', 'Negative', 'Neutral', '']);
//            dd($results);
            $data['results'] = $results;
            $data['current_search'] = '';
            $date = new \DateTime();
            $date->setDate(2017, 01, 1);
            $data['start_date'] = $date->format('m/d/Y');
            $data['current_date'] = date('m/d/Y');
            $data['current_city'] = '';
            $data['current_search'] = '';
            $data['current_source'] = '';
            $data['selected_sources'] = [];

            $data['keywords'] = Articles::getTrendingKeywords(Carbon::now()->subDays(30)->toIso8601String(), Carbon::now()->toIso8601String(), null);
            $data['keywords'] = array_slice($data['keywords'], 0, 10);
//        dd(array_values($results));
        } else {
//            dd($request->all());
//            dd($request->daterange);

            $date_range = $request->daterange;
            $date_range = explode("-", $date_range);
            $date_part_1 = new Carbon($date_range[0]);
            $date_part_2 = new Carbon($date_range[1]);
            if ($request->search_source == null) {
                $search_sources = '';
            } else {
                $search_sources = explode(',', $request->search_source);
            }

//            dd($search_sources);
            //Need to define combinations here!!!
            $articles = Articles::getAnalysisText($request_type, $request->search, $date_part_1->format('Y-m-d H:i:s'), $date_part_2->format('Y-m-d H:i:s'), $search_sources);
//            dd($articles);
            $sources = [];
            $results = Array();
            foreach ($articles as $rd) {
//            $sources[]=
                $sources[$rd['_id']] = [
                    'source' => $rd['_id'],
                    isset($rd['values'][0]['sentiment']) ? $rd['values'][0]['sentiment'] : '' =>
                        isset($rd['values'][0]['sentiment']) ? $rd['values'][0]['count'] : '',
                    isset($rd['values'][1]['sentiment']) ? $rd['values'][1]['sentiment'] : '' =>
                        isset($rd['values'][1]['sentiment']) ? $rd['values'][1]['count'] : '',
                    isset($rd['values'][2]['sentiment']) ? $rd['values'][2]['sentiment'] : '' =>
                        isset($rd['values'][2]['sentiment']) ? $rd['values'][2]['count'] : '',
                ];
            }
//        dd($sources);
//            $sources = array_slice($sources, 0, 12);
//            $final_sources = [];
//            foreach ($sources as $key => $sr) {
//                if (in_array($key, $search_sources))
//                    $final_sources[] = $key;
//            }
//            $sources = $final_sources;
//            dd($sources);
            $results = [];
            foreach ($sources as $rd) {
                $negative = isset($rd['positive']) ? $rd['positive'] : 0;
                $positive = isset($rd['negative']) ? $rd['negative'] : 0;
                $neutral = isset($rd['neutral']) ? $rd['neutral'] : 0;
                $sum = $negative + $positive + $neutral;
                $results[] = [isset($rd['source']) ? $rd['source'] : '', isset($rd['positive']) ? $rd['positive'] : 0
                    , isset($rd['negative']) ? $rd['negative'] : 0, isset($rd['neutral']) ? $rd['neutral'] : 0, $sum];
            }
            usort($results, function ($b, $a) {
                return $a['4'] - $b['4'];
            });
            array_unshift($results, ['Source', 'Positive', 'Negative', 'Neutral', '']);
            $data['results'] = $results;
            $data['current_search'] = $request->search;
            $data['start_date'] = $date_range[0];
            $data['current_date'] = $date_range[1];
            $data['current_city'] = '';
            $data['current_source'] = '';

            if ($request->search_source == null) {
                $data['selected_sources'] = [];
            } else {
                $data['selected_sources'] = $search_sources;
            }

//            $data['current_search'] = '';

            $data['keywords'] = Articles::getTrendingKeywords($date_part_1->toIso8601String(), $date_part_2->toIso8601String(), $request->search, $search_sources);
            $data['keywords'] = array_slice($data['keywords'], 0, 10);
        }

//        if ($request->type == 'source.name') {
//            $data['search_group'] = 'source.name';
//        } else if ($request->type == 'source.location') {
//            $data['search_group'] = 'source.location';
//        } else {
//            $data['search_group'] = 'sentiment.value';
//        }


        $data['cities'] = Articles::distinct('source.location')->get()->toArray();
        $data['sources'] = Articles::distinct('source.name')->get()->toArray();

        return view('analysis.new_chart', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function mapview()
    {
//        dd(\Illuminate\Support\Facades\Route::current()->uri());
        $data['news'][] = ['Country', 'News Articles'];
        $news = Articles::getGroupByCount('location');
        $final_news = [];
        $final_news[] = ['Country', 'News Articles'];
        foreach ($news as $rd) {
            if ($rd['_id'] == 'UK')
                $rd['_id'] = 'United Kingdom';
//                dd();
            $final_news[] = array_values($rd);
        }
//        dd($final_news);
        $data['final_news'] = $final_news;

//        dd('here');
        return view('analysis.map', $data);
    }

}
