<?php

namespace FeedMeNews\Http\Controllers;

use FeedMeNews\Comments;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->userId && $request->newsId) {
//            dd('reached');
            $user_id = $request->userId;
            $news_id = $request->newsId;
            $comment = Comments::where('userId', '=', $user_id)
                ->where('newsArticleId', '=', $news_id)
                ->get();
        } else if ($request->userId) {
            $user_id = $request->userId;
            $comment = Comments::where('userId', '=', $user_id)
                ->get();
        } else if ($request->newsId) {
            $news_id = $request->newsId;
            $comment = Comments::where('newsArticleId', '=', $news_id)
                ->get();
        } else {
            $comment = Comments::all();
        }

        return responseResult($comment);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data['publishDate'] = date('Y-m-d H:i:s');
        $comment = Comments::create($data);

        return responseResult($comment);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comments::find($id);
        return responseResult($comment);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
//        dd($data);
        $comment = Comments::findorFail($id);
//        dd($user);
        $comment->update([
            'text' => $data['text'],
        ]);
        return responseResult($comment);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->userId && $request->newsId) {
//            dd('user & News: ' . $request->userId . ' - ' . $request->newsId);
            $user_id = $request->userId;
            $news_id = $request->newsId;
            $comments = Comments::where('userId', '=', $user_id)
                ->where('newsArticleId', '=', $news_id)
                ->get();
//            dd($comments);
            foreach ($comments as $rev) {
                $rev->delete();
            }
        } else if ($request->userId) {
//            dd('user: ' . $request->userId);
            $user_id = $request->userId;
            $comments = Comments::where('userId', '=', $user_id)
                ->get();
//            dd($comments);
            foreach ($comments as $rev) {
                $rev->delete();
            }
        } else if ($request->newsId) {
//            dd('News: ' . $request->newsId);
            $news_id = $request->newsId;
            $comments = Comments::where('newsArticleId', '=', $news_id)
                ->get();
//            dd($comments);
            foreach ($comments as $rev) {
                $rev->delete();
            }
        } else {
//            dd('comment Id: ' . $id);
            Comments::destroy($id);
        }
        //
        return responseResult('Comment Deleted Successfully!');
    }
}
