<?php
/**
 * Created by PhpStorm.
 * User: hyds
 * Date: 11/28/2017
 * Time: 11:44 PM
 */


return [
    'Russia Today' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Russia-today-logo.svg/302px-Russia-today-logo.svg.png',
    'BBC UK' => 'http://ziferblat.net/wp-content/uploads/2014/01/67373987_09f1654a-e583-4b5f-bfc4-f05850c6d3ce.jpg',
    'Moscow Times' => 'https://crunchbase-production-res.cloudinary.com/image/upload/c_lpad,h_256,w_256,f_jpg/v1409913071/s2hponbbh9lc0tuzfowg.png',
    'Time' => 'https://s0.wp.com/wp-content/themes/vip/time2014/img/time-logo-og.png',
    'Russia Insider' => 'https://image.roku.com/developer_channels/prod/91f5552fc575dcc2470a091154ee16af6d11ebd9dcd4d650418d063daac3b4e7.png',
    'Reuters' => 'http://blog.ourcrowd.com/wp-content/uploads/2014/03/Reuters-Logo.jpg',
    'Shanghai Daily' => 'http://www.talent-fishers.com/website/wp-content/uploads/2016/09/%E4%B8%8A%E6%B5%B7%E6%97%A5%E6%8A%A5Shanghai-Daily_Logo.jpg',
    'The Guardian' => 'http://mizbala.com/_uploads/images/2017/04/the-guardian.jpg',
    'The Hill' => 'http://www.cgcn.com/wp-content/uploads/2016/08/the-hill.png',
    'The Local DE' => 'https://www.thelocal.de/assets/de/image/the-local-de.svg',
    'The Local ES' => 'https://www.thelocal.es/assets/es/image/the-local-es.svg',
    'The Local FR' => 'https://www.thelocal.fr/assets/fr/image/the-local-fr.svg',
    'VoxEurop' => 'http://www.citizensforeurope.eu/wp-content/uploads/2015/04/VoxEurop.png',
    'Sky News' => 'http://kashgt.com/wp-content/uploads/2014/06/sky-news-logo.png',
    'Tass' => 'http://sovethr.ru/wp-content/uploads/2017/10/22.png',
    'Sputnik' => 'http://is4.mzstatic.com/image/thumb/Purple128/v4/1d/9c/84/1d9c8424-6c95-db7c-30e1-1ef614cf3f05/source/1200x630bb.jpg',
    'Mirror' => 'https://lh3.googleusercontent.com/MDO8iNBCRl_94UrF7Gfp1nY6Pb3V-u7JKsAwdLZNK5zo9qD2QdCvtFF9gjAMgx7KnA=w300',
    'El Mundo' => 'http://www.unidadeditorial.com/publicidad/images/logos/prensa/elmundo/Logo_Elmundo.jpg',
    'EuroNews' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Euronews_2016_logo.svg/2000px-Euronews_2016_logo.svg.png',
    'CNN' => 'http://1000logos.net/wp-content/uploads/2016/11/CNN-Logo.png',
    'France24' => 'http://www.delorsinstitute.eu/images/bibli/logo-france24.jpg_574_3000_2',
    'Fox News' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/67/Fox_News_Channel_logo.svg/1200px-Fox_News_Channel_logo.svg.png',
    'EUObserver' => 'https://euobserver.com/graphics/logo.svg',
    'ElPais' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/El_Pais_logo_2007.svg/2000px-El_Pais_logo_2007.svg.png',
    'Euractiv' => 'http://www.euractiv.com/wp-content/uploads/sites/2/2017/02/EURACTIV_LOGO_ORIGINAL_RGB_XL-1.png',
    'Meduza' => 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Meduza_logo.svg/1024px-Meduza_logo.svg.png',
    'Politico' => 'https://static.politico.com/51/39/958e25a2475dafd4f496e5eecf1e/politics-policy-political-news.jpg',
    'Pravda Report' => 'http://pravda-team.ru/eng/image/article/9/0/8/57908.jpeg',
    'RFE' => 'https://upload.wikimedia.org/wikipedia/en/8/81/RFE_logoFC_wiki.jpg',
    'Real Clear Politics' => 'https://www.realclearpolitics.com/images/realclearpolitics_social_logo.png',
    'Spiegel' => 'https://upload.wikimedia.org/wikipedia/commons/3/3e/Logo-der_spiegel.svg',
];