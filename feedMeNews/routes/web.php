<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//});

//Social Auth
Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');


Route::resource('/', 'ArticlesController');
Route::resource('analysis', 'AnalysisController');
Route::get('mapview', 'AnalysisController@mapview');
Route::resource('search', 'SearchController');
Route::resource('user', 'UserController');
Route::resource('account', 'AccountController');
//Route::resource('account', 'SentimentsController');
Route::resource('mongo', 'MongoConnectController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/api/v1/articles', function () {
//    $count = \FeedMeNews\Articles::count();
//    dd($count);
    $rand = rand(0, 10);
    return \FeedMeNews\Articles::take(10)->skip($rand)->get();
});


$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['prefix' => 'api/v1'], function ($api) {

    $api->get('/users/login', function (\Illuminate\Http\Request $request) {
//
//    dd('here');
        $email = $request->get('email');
        $password = $request->get('password');
//        dd($password);
//    dd(User::all());
        $user = \FeedMeNews\User::where('encodedPassword', '=', $password)->where('email', '=', $email)->first();
        $info = array();
//        dd(getRandomString(80));
        if ($user) {
            $info['info'] = 'Welcome ' . $user->firstName . ' ' . $user->lastName;
            $info['email'] = $user->email;

            if ($user->token && $user->token != '') {
                $info['token'] = $user->token;
            } else {
                $faker = Faker\Factory::create();
//            $faker->text(200);
                $user->update([
                    'token' => getRandomString(80)
                ]);
                $info['token'] = $user->token;
            }
            //$user
            $info['status'] = 'Successfully logged in!';
        } else {
//    dd($userNode);
            $info['status'] = 'Login Error!, Please check your Email & Password!';
        }

        return responseResult($info);
    });

    //Logout User
    $api->get('/users/logout', function (\Illuminate\Http\Request $request) {
        $token = $request->get('access_token');
//        dd('here');
        $user = \FeedMeNews\User::where('token', '=', $token)->first();
//        $user->delete();
//        dd($user);
        if ($user && $user->token != '' && $user->token != null) {
            $user->update(['token' => '']);
            $info['info'] = 'You are successfully log out';
        } else {
            $info['info'] = 'Please provide the right token for Log Out!';
        }

        //remove token from the user field, update user with empty field
        $info['status'] = 'Logout!';
        return responseResult($info);
    });


    //General Functionalities
    $api->group(['middleware' => ['apiAuth']], function ($api) {
        $api->get('users/{id}/comments', 'FeedMeNews\Http\Controllers\UserController@comments');
        $api->get('users/{id}/ratings', 'FeedMeNews\Http\Controllers\UserController@reviews');

        $api->get('articles/{id}/comments', 'FeedMeNews\Http\Controllers\ArticlesApiController@comments');
        $api->get('articles/{id}/ratings', 'FeedMeNews\Http\Controllers\ArticlesApiController@reviews');

        $api->resource('users', 'FeedMeNews\Http\Controllers\UserController');
        $api->resource('ratings', 'FeedMeNews\Http\Controllers\ReviewController');
        $api->resource('comments', 'FeedMeNews\Http\Controllers\CommentController');
        $api->resource('articles', 'FeedMeNews\Http\Controllers\ArticlesApiController');
    });


});