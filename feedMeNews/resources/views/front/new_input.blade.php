<div class="content">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{--<a href="#">Brand</a>--}}
                <a class="navbar-brand" href="{{url('/')}}"><img width="130" style="margin-top: -8px;" height="60"
                                                                 src="{{asset('images/logo.png')}}"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                {!! Form::open(['url' => '/', 'method' => 'get','id'=>'search_form','class'=>'navbar-form navbar-left']) !!}
                <div class="input-group" style="margin-top:14px;">
                    <input id="search_input" name="search" type="text" value="{{$current_search ? $current_search:''}}"
                           class="form-control" placeholder="#Keywords" style="width: 200px;margin-left: 14px">
                    <input id="search_city" name="search_city" type="hidden"
                           value="{{$current_city ? $current_city:''}}">
                    <input id="search_source" name="search_source" type="hidden"
                           value="{{$current_source ? $current_source:''}}">
                    <input id="sentiment_group" name="sentiment_group" type="hidden" value="{{$sentiment}}">
                    <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><span
                                        class="glyphicon glyphicon-search"></span></button>
                        </span>
                </div>

                <input type="text" name="daterange" class="form-control"
                       style="margin-top:13px;margin-left:5px;width:185px"
                       value="{{$start_date}} - {{$current_date}}"/>
                {!! Form::close() !!}
                {{--<div>--}}
                <ul class="nav navbar-nav">

                    <li class="dropdown" style="margin-top: 3px;margin-left: -10px">
                        <a href="#" id="location_dropdown" class="dropdown-toggle" data-toggle="dropdown"
                           role="button"
                           aria-haspopup="true"
                           aria-expanded="false">{{$current_city ? $current_city:'Source Location'}}<span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu">
                            @foreach($cities as $city)
                                <li><a class="dropdown-item search_city" href="#">{{$city[0]}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
                {{--<div class="">--}}
                {{--<label id="source_dropdown" for="sel1">{{$current_source ? $current_source:'Source Name'}}</label>--}}
                {{--<select multiple class="select2" id="sel2">--}}
                {{--@foreach($sources as $source)--}}
                {{--<option class="search_source" href="#">{{$source[0]}}</option>--}}
                {{--@endforeach--}}
                {{--</select>--}}
                {{--</div>--}}
                <ul class="nav navbar-nav">
                    <li class="dropdown" style="margin-top: 21px">
                        <select id="news-source-select" multiple="multiple">
                            @foreach($sources as $source)
                                {{--<li><a class="dropdown-item search_source" href="#">{{$source[0]}}</a></li>--}}
                                @if(in_array($source[0], $selected_sources) || $selected_sources==[])
                                    <option value="{{$source[0]}}" selected="selected"
                                            class="search_source">{{$source[0]}}</option>
                                @else
                                    <option value="{{$source[0]}}"
                                            class="search_source">{{$source[0]}}</option>
                                @endif

                            @endforeach
                        </select>
                    </li>
                    {{--<li class="dropdown">--}}
                    {{--<a href="#" id="source_dropdown" class="dropdown-toggle" data-toggle="dropdown"--}}
                    {{--role="button"--}}
                    {{--aria-haspopup="true"--}}
                    {{--aria-expanded="false">{{$current_source ? $current_source:'Source Name'}}<span--}}
                    {{--class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                    {{--@foreach($sources as $source)--}}
                    {{--<li><a class="dropdown-item search_source" href="#">{{$source[0]}}</a></li>--}}
                    {{--@endforeach--}}

                    {{--</ul>--}}
                    {{--</li>--}}
                </ul>
                <ul class="nav navbar-nav">

                    <li class="dropdown"  style="margin-top: 5px">
                        <a href="#" id="sentiment_group_dropdown" style="text-transform: capitalize;"
                           class="dropdown-toggle" data-toggle="dropdown"
                           role="button"
                           aria-haspopup="true"
                           aria-expanded="false">{{ $sentiment ?  $sentiment:'Sentiment'}}<span
                                    class="caret"></span></a>
                        <ul class="dropdown-menu">

                            <li><a class="dropdown-item sentiment_group" style="text-transform: capitalize;" href="#">positive</a>
                            </li>
                            <li><a class="dropdown-item sentiment_group" style="text-transform: capitalize;" href="#">negative</a>
                            </li>
                            <li><a class="dropdown-item sentiment_group" style="text-transform: capitalize;" href="#">neutral</a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    @if (Route::has('login'))
                        @if (Auth::check())
                            <li><a href="{{ url('/home') }}">Account</a></li>
                        @else
                            <li  style="margin-top: 5px"><a href="{{ url('/analysis') }}">Analytics</a></li>
                            <li style="margin-top: 5px"><a href="{{ url('/mapview') }}">MapView</a></li>
                            <li style="margin-top: 5px"><a href="{{ url('/login') }}">Login</a></li>
                            <li style="margin-top: 5px"><a href="{{ url('/register') }}">Register</a></li>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                @foreach($keywords as $key)
                    <a style="margin-right: 2px;margin-bottom: 5px;margin-top: -10px;padding: 3px;font-size: 16px" type="button" href="#"
                       class="btn btn-sm btn-info keywords">#{{$key['_id']}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                @endforeach
            </div>
        </div>
    </div>

</div>