<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    @include('header')
    <style>
        .nav > li > a {
            padding: 10px 8px;
        }

        .label-default {
            /*padding:0px;*/
            color: #333;
            background-color: white;
        }

        .label-default:hover {
            background-color: #5e5e5e;
            color: white;
        }
    </style>
</head>
<body>
@include('front.new_input')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h5>Results - {{$articles->firstItem()}} of {{$articles->total()}} Articles</h5>
            <hr>
            @if(!$articles->isEmpty())
                @foreach($articles as $article)
                    <div class="panel-group">
                        @if($article->sentiment['value']=='negative')
                            <div class="panel panel-danger">
                                @elseif($article->sentiment['value']=='positive')
                                    <div class="panel panel-success">
                                        @elseif($article->sentiment['value']=='neutral')
                                            <div class="panel panel-info">
                                                @endif

                                                <div class="panel-heading"></div>
                                                <div class="panel-body form-inline" style="padding: 10px;">
                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            <a href="{{$article->url}}" target="_blank"><img width="90"
                                                                                                             height="50"
                                                                                                             src="{{loadLogo($article->source['name'])}}"></a>
                                                        </div>
                                                        <div class="col-md-11">
                                                            <a href="{{$article->url}}" target="_blank">
                                                                <h5 style="font-weight:bold;font-size: 24px;margin-top: -1px;">{{strip_tags($article->title)}}</h5>
                                                            </a>
                                                            <div style="margin-top: -3px;">
                                                                <span>{{ $article->source['name']}}</span>
                                                                |
                                                                <span>{{$article->source['location']}}</span>
                                                                |
                                                                <span>{{ \Carbon\Carbon::parse($article->publishDate)}}</span>
                                                                |
                                                                @if($article->sentiment['value']=='negative')
                                                                    <span class="label label-danger"
                                                                    >{{ $article->sentiment['value']}}</span>
                                                                @elseif($article->sentiment['value']=='positive')
                                                                    <span class="label label-success"
                                                                    >{{ $article->sentiment['value']}}</span>
                                                                @elseif($article->sentiment['value']=='neutral')
                                                                    <span class="label label-info"
                                                                    >{{ $article->sentiment['value']}}</span>
                                                                @endif

                                                            </div>

                                                        </div>
                                                        <div class="col-md-12" style="margin-top: 8px;">
                                                            <p style="">{{strip_tags(str_limit($article->description, 600))}}</p>
                                                            <?php $count = 0; ?>
                                                            @foreach($article->keywords as $key=>$word)
                                                                @if($count==10)
                                                                    @break
                                                                @endif
                                                                @if(strlen($word)>4)
                                                                    <a class="label label-default"
                                                                       style="font-size: 12px;margin-right: 15px;"
                                                                       href="#"
                                                                    >#{{$word}}</a>
                                                                    <?php $count++; ?>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>

                                    {{--<hr>--}}
                                    @endforeach
                                    @if($articles!=null)
                                        {{ $articles->appends(['search' => $current_search,'search_city'=>$current_city,'search_source'=>$current_source,'daterange'=>$date_range,'sentiment_group'=>$sentiment,])->links() }}
                                    @endif
                                @else
                                    <p>No Results Found with your query, Please try again changing
                                        your search!!</p>
                                @endif

                                {{--</div>--}}
                                {{--<div class="col-4">--}}
                                {{--@include('sidebar')--}}

                                {{--</div>--}}
                            </div>
                            <div class="row">
                                <div class="col-8">


                                </div>

                            </div>
                    </div>


        </div>
        <script type="text/javascript">
            $(function () {
                $('input[name="daterange"]').daterangepicker();
            });

            $('.select2').select2();


            $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
                console.log(picker.startDate.format('YYYY-MM-DD'));
                console.log(picker.endDate.format('YYYY-MM-DD'));
                $('#search_form')[0].submit();
            });
            $(".keywords").click(function () {
//        alert($(this).text().substring(1));
                $('#search_input').val($(this).text().substring(1));
                $('#search_form')[0].submit();
            });
            $('.search_city').click(function () {
                $('#location_dropdown').text($(this).text());
                $('#search_city').val($(this).text());
                $('#search_form')[0].submit();
            });
            $('.search_source').click(function () {
                $('#source_dropdown').text($(this).text());
                $('#search_source').val($(this).text());
                $('#search_form')[0].submit();
            });

            $('.sentiment_group').click(function () {
                $('#sentiment_group_dropdown').text($(this).text());
                $('#sentiment_group').val($(this).text());
                $('#search_form')[0].submit();
            });


            $('.pagination li').addClass('page-item');
            $('.pagination li a').addClass('page-link');
            $('.pagination li span').addClass('page-link');

            //            $('#example-getting-started').multiselect();
            $('#news-source-select').multiselect({
                includeSelectAllOption: true,
                selectAllText: 'Sources',
                allSelectedText: "Sources",
                onSelectAll: function () {
//                    alert('onSelectAll triggered!');
                    $('#search_source').val(null);
                    $('#search_form')[0].submit();

                },
                onChange: function (element, checked) {
                    var brands = $('#news-source-select option:selected');
                    var selected = [];
                    $(brands).each(function (index, brand) {
                        selected.push($(this).val());
                    });
//                    console.log(selected);
                    $('#search_source').val(selected);
                    $('#search_form')[0].submit();
                }
            });
        </script>
</body>
</html>
