<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FeedMeNews</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>

    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
</head>
<body>
<div class="container-lg" style="margin-top: 5px;">
    <!-- Content here -->
    <div class="row">
        {{--<div class="offset-md-1 col-md-2" style="">--}}
        <a style="margin-left: 80px" href="{{url('/')}}"><img src="{{asset('images/logo.png')}}"></a>
        {{--</div>--}}
        {{--<div style="margin-left: 50px">--}}
        @include('front.input')
        {{--</div>--}}
        {{--<div class="col-md-1">--}}
        @if (Route::has('login'))
            <div style="margin-left: 70px;margin-top: 20px;">
                @if (Auth::check())
                    <a href="{{ url('/home') }}">Home</a>
                @else
                    <a href="{{ url('/login') }}" class="text-info">Login</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    &nbsp;
                    &nbsp;&nbsp;
                    <a href="{{ url('/register') }}" class="text-info">Register</a>
                @endif
            </div>
        @endif
        {{--</div>--}}

    </div>

</div>
<div class="container">
    <div class="row">
        <div class="offset-md-1 col-md-10 ">
            @foreach($keywords as $key)
                <a style="margin-right: 2px;margin-bottom: 10px;" type="button" href="#"
                   class="btn btn-sm btn-outline-info keywords">#{{$key['_id']}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
            @endforeach
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="offset-md-1 col-md-10 ">
            <h5>Results - 20 of {{$total_articles}} Articles</h5>
            <hr>
            @if(!$articles->isEmpty())
                @foreach($articles->sortByDesc('date') as $article)
                    <div class="card bg-light border-info">
                        <div class="card-body text-info">
                            <div class="row">
                                <div class="col-2">
                                    <a href="{{$article->url}}" target="_blank"><img width="140" height="75"
                                                                                     src="{{config('news.'.$article->source['name'])}}"></a>
                                </div>
                                <div class="col-8">
                                    <a href="{{$article->url}}" target="_blank"><h5
                                                class="card-title text-info">{{$article->title}}</h5></a>
                                    {{--<span>932k followers / 232 articles</span>--}}
                                    <span style="color: red;">{{$article->source['location']}}</span> | <span
                                            style="color: green;">{{ Carbon\Carbon::parse($article->publishDate)->diffForHumans()}}</span>
                                    | <span
                                            style="color: red;">{{ $article->sentiment['value']}}</span>
                                    | <span
                                            style="color: red;">{{ $article->source['name']}}</span>
                                    {{--<a target="_blank" href="{{$article->url}}"><p--}}
                                    {{--class="card-title text-danger">{{$article->source['name']}}</p></a>--}}
                                </div>
                                <div class="col-md-12">
                                    <p class="card-text">{{str_limit($article->description, 200)}}</p>
                                    <div style="margin-top: -10px">
                                        @foreach(array_slice($article->keywords, 0, 10) as $word)

                                            <a href="#" class="badge badge-info">#{{$word}}</a>&nbsp;&nbsp;
                                            &nbsp;&nbsp;
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            {{--<div class="row">--}}
                            {{----}}
                            {{--</div>--}}

                        </div>
                    </div>
                    <hr>
                @endforeach
                {{--@if($articles!=null)--}}
                {{--{{ $articles->links() }}--}}
                {{--@endif--}}
            @else
                <p>No Results Found with your query, Please try again changing your search!!</p>
            @endif

        </div>
        {{--<div class="col-4">--}}
        {{--@include('sidebar')--}}

        {{--</div>--}}
    </div>
    <div class="row">
        <div class="col-8">


        </div>

    </div>
</div>


</div>
<script type="text/javascript">
    $(function () {
        $('input[name="daterange"]').daterangepicker();
    });

    $(".keywords").click(function () {
//        alert($(this).text().substring(1));
        $('#search_input').val($(this).text().substring(1));
        $('#search_form')[0].submit();
    });
    $('.search_city').click(function () {
        $('#location_dropdown').text($(this).text());
        $('#search_city').val($(this).text());
        $('#search_form')[0].submit();
    });
    $('.pagination li').addClass('page-item');
    $('.pagination li a').addClass('page-link');
    $('.pagination li span').addClass('page-link');
</script>
</body>
</html>
