{{--<div class="row">--}}
{{--<div class="col-md-4">--}}
{!! Form::open(['url' => '/', 'method' => 'get','id'=>'search_form','class'=>'form-inline','style'=>'margin-left:80px']) !!}


<div class="form-group">
    <input id="search_input" name="search" type="text" class="form-control" placeholder="#Keywords"

           aria-label="Search for..." value="{{$current_search ? $current_search:''}}">
    <input id="search_city" name="search_city" type="hidden" value="{{$current_city ? $current_city:''}}">
    <span class="input-group-btn">
        <button class="btn btn-info" type="submit">Search!</button>
                </span>
</div>
&nbsp;&nbsp;
{{--</div>--}}
{{--<div class="col-md-3">--}}
<div class="dropdown">
    <button class="btn btn-info dropdown-toggle" type="button" id="location_dropdown"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{$current_city ? $current_city:'Source Location'}}
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        @foreach($cities as $city)
            <a class="dropdown-item search_city" href="#">{{$city[0]}}</a>
        @endforeach
    </div>
</div>
&nbsp;&nbsp;
<div class="dropdown">
    <button class="btn btn-info dropdown-toggle" type="button" id="location_dropdown"
            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{$current_city ? $current_city:'Source Name'}}
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        @foreach($cities as $city)
            <a class="dropdown-item search_city" href="#">{{$city[0]}}</a>
        @endforeach
    </div>
</div>
&nbsp;&nbsp;
<div>
    <input type="text" name="daterange" class="form-control" style="width:293px"
           value="{{$start_date}} - {{$current_date}}"/>
</div>
&nbsp;&nbsp;
{{--<div class="dropdown">--}}
{{--<button class="btn btn-info dropdown-toggle" type="button" id="group_dropdown"--}}
{{--data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--Group--}}
{{--</button>--}}
{{--<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">--}}

{{--<a class="dropdown-item search_city" href="{{url('account?type=name')}}">Sources</a>--}}
{{--<a class="dropdown-item search_city" href="{{url('account?type=location')}}">Location</a>--}}
{{--<a class="dropdown-item search_city" href="{{url('account?type=sentiment')}}">Sentiment</a>--}}

{{--</div>--}}
{{--</div>--}}
{{--</div>--}}

{{--</div>--}}
    {!! Form::close() !!}