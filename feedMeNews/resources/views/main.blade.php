<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FeedMeNews</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css"
          integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">


    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js"
            integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh"
            crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"
            integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ"
            crossorigin="anonymous"></script>
</head>
<body>
<div class="container">
    <!-- Content here -->
    <div class="row">
        <div class="col-md-12" style="margin-top: 50px;">
            <img src="{{asset('images/logo.png')}}">
            @if (Route::has('login'))
                <div style="float: right;">

                    <a href="{{ url('/register') }}" class="text-info">Account & Settings</a>

                </div>
            @endif
        </div>
    </div>
    <br><br>
    <div class="row">
        <div class="col-8">
            @include('input')
            <h3>Results (Top 10)</h3>
            <hr>
            <div class="card bg-light border-success">
                <div class="card-header bg-success text-white">Positive</div>
                <div class="card-body text-success">
                    <div class="row">
                        <div class="col-2">
                            <img width="110" height="75"
                                 src="http://mapecology.ma/wp-content/uploads/2016/11/diario-el-pais-582x386.jpg">
                        </div>
                        <div class="col-8">
                            <h4 class="card-title">El Pais</h4>
                            <span>932k followers / 232 articles</span>
                            <p class="card-text">Noticias de última hora sobre la actualidad en España y el mundo</p>
                            <a href="#" class="badge badge-info">#politics</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#science</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#travel</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#education</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-2">
                            <img width="110" height="75"
                                 src="http://ziferblat.net/wp-content/uploads/2014/01/67373987_09f1654a-e583-4b5f-bfc4-f05850c6d3ce.jpg">
                        </div>
                        <div class="col-8">
                            <h4 class="card-title">BBC News</h4>
                            <span>932k followers / 232 articles</span>
                            <p class="card-text">Noticias de última hora sobre la actualidad en España y el mundo</p>
                            <a href="#" class="badge badge-info">#politics</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#science</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#travel</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#education</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>

                </div>
            </div>
            <br>
            <div class="card bg-light border-danger">
                <div class="card-header bg-danger text-white">Negative</div>
                <div class="card-body text-danger">
                    <div class="row">
                        <div class="col-2">
                            <img width="110" height="75"
                                 src="http://mapecology.ma/wp-content/uploads/2016/11/diario-el-pais-582x386.jpg">
                        </div>
                        <div class="col-8">
                            <h4 class="card-title">El Pais</h4>
                            <span>932k followers / 232 articles</span>
                            <p class="card-text">Noticias de última hora sobre la actualidad en España y el mundo</p>
                            <a href="#" class="badge badge-info">#politics</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#science</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#travel</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#education</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-2">
                            <img width="110" height="75"
                                 src="http://ziferblat.net/wp-content/uploads/2014/01/67373987_09f1654a-e583-4b5f-bfc4-f05850c6d3ce.jpg">
                        </div>
                        <div class="col-8">
                            <h4 class="card-title">BBC News</h4>
                            <span>932k followers / 232 articles</span>
                            <p class="card-text">Noticias de última hora sobre la actualidad en España y el mundo</p>
                            <a href="#" class="badge badge-info">#politics</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#science</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#travel</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#education</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="card bg-light border-info">
                <div class="card-header bg-info text-white">Neutral</div>
                <div class="card-body text-info">

                    <div class="row">
                        <div class="col-2">
                            <img width="110" height="75"
                                 src="http://1000logos.net/wp-content/uploads/2016/11/CNN-Logo.png">
                        </div>
                        <div class="col-8">
                            <h4 class="card-title">CNN News</h4>
                            <span>932k followers / 232 articles</span>
                            <p class="card-text">Noticias de última hora sobre la actualidad en España y el mundo</p>
                            <a href="#" class="badge badge-info">#politics</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#science</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#travel</a>&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="#" class="badge badge-info">#education</a>&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <div class="col-4">
            @include('sidebar')

        </div>
    </div>
    <div class="row">
        <div class="col-8">


        </div>

    </div>

</div>


</div>
</body>
</html>
