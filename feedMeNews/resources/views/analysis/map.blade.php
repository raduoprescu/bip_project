<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>

    @include('header')
    <title>FeedMeNews Analytics</title>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>
<body>
@include('analysis.chart_input')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h4>Total Fetched New Articles for Countries</h4>
            <hr>
            {{--<br>--}}
            <div id="regions_div" style="width: 900px; height: 500px;"></div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#news-source-select').multiselect({
        includeSelectAllOption: true,
        selectAllText: 'Sources',
        allSelectedText: "Sources",
        onSelectAll: function () {
//                    alert('onSelectAll triggered!');
            $('#search_source').val(null);
            $('#search_form')[0].submit();

        },
        onChange: function (element, checked) {
            var brands = $('#news-source-select option:selected');
            var selected = [];
            $(brands).each(function (index, brand) {
                selected.push($(this).val());
            });
//                    console.log(selected);
            $('#search_source').val(selected);
            $('#search_form')[0].submit();
        }
    });

    $(function () {
        $('input[name="daterange"]').daterangepicker();
    });
    var APP_URL = {!! json_encode(url('/')) !!}
    //    var sentiment_type = 'Sources';


    var map_data = {!! json_encode($final_news) !!};


    google.charts.load('current', {
        'packages': ['geochart'],
        // Note: you will need to get a mapsApiKey for your project.
        // See: https://developers.google.com/chart/interactive/docs/basic_load_libs#load-settings
        'mapsApiKey': 'AIzaSyD-9tSrke72PouQMnMX-a7eZSW0jkFMBWY'
    });
    google.charts.setOnLoadCallback(drawRegionsMap);

    function drawRegionsMap() {
        var data = google.visualization.arrayToDataTable(map_data);

        var options = {};

        var chart = new google.visualization.GeoChart(document.getElementById('regions_div'));

        chart.draw(data, options);
    }
</script>
</body>
</html>
