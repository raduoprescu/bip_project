<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>

    @include('header')
    <title>FeedMeNews Analytics</title>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

</head>
<body>
@include('analysis.chart_input')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {{--<h4>Analysis Results</h4>--}}
            {{--<hr>--}}
            <div id="chart_div" style="margin-top: -10px"></div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $('#news-source-select').multiselect({
        includeSelectAllOption: true,
        selectAllText: 'Sources',
        allSelectedText: "Sources",
        onSelectAll: function () {
//                    alert('onSelectAll triggered!');
            $('#search_source').val(null);
            $('#search_form')[0].submit();

        },
        onChange: function (element, checked) {
            var brands = $('#news-source-select option:selected');
            var selected = [];
            $(brands).each(function (index, brand) {
                selected.push($(this).val());
            });
//                    console.log(selected);
            $('#search_source').val(selected);
            $('#search_form')[0].submit();
        }
    });

    $(function () {
        $('input[name="daterange"]').daterangepicker();
    });
    var APP_URL = {!! json_encode(url('/')) !!}
        //    var sentiment_type = 'Sources';

        $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));
            $('#search_form')[0].submit();
        });

    $('.type_group').click(function () {
//        sentiment_type = $(this).text();
//        text_val = $(this).text().replace('By ', '');
//        console.log(sentiment_type);
        $('#group_dropdown').text($(this).text());
        $('#search_group').val($(this).text());
        $('#search_form')[0].submit();
    });

    $(".keywords").click(function () {
//        alert($(this).text().substring(1));
        $('#search_input').val($(this).text().substring(1));
        $('#search_form')[0].submit();
    });
    $('.pagination li').addClass('page-item');
    $('.pagination li a').addClass('page-link');
    $('.pagination li span').addClass('page-link');


    var chart_data = {!! json_encode($results) !!};

    var sentiment_type = {!! json_encode($type_group_chart) !!};

    if (sentiment_type == null || sentiment_type == '')
        sentiment_type = 'Sources';
    //    alert(sentiment_type);
    var date_range = $('input[name="daterange"]').val();
    var text_search = $('#search_input').val();
    //    console.log(date_range);

    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawStacked);

    function drawStacked() {
        var data = google.visualization.arrayToDataTable(chart_data);
        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1, 2, 3,
            {
                calc: "stringify",
                sourceColumn: 4,
                type: "string",
                role: "annotation"
            }]);
        var options = {
            title: 'Articles based on sources categorized by sentiment',
//            chartArea: {width: '60%'},
            width: 1200,
            height: 650,
            colors: ['#5CB85C', '#D9534F', '#5BC0DE', '#000000'],
            bar: {groupWidth: '50%'},
            legend: {position: 'top', maxLines: 3},
            isStacked: true,
            hAxis: {
                title: 'Number of Articles',
                minValue: 0,
            },
            vAxis: {
                title: 'Sources'
            },
            annotations: {
                textStyle: {
                    fontSize: 14
                },
//                color: "black",
            },
        };
        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
        chart.draw(view, options);

        google.visualization.events.addListener(chart, 'select', selectHandler);

// The select handler. Call the chart's getSelection() method
        function selectHandler() {
//            alert('here');
            var selection = chart.getSelection();
            var message = '';

            for (var i = 0; i < selection.length; i++) {
                var item = selection[0];
                if (item.row != null && item.column != null) {
                    var str = item.column;
                    var chart_value = data.getValue(chart.getSelection()[0].row, 0);

                }
            }
            //make decision on group by, location
            var type = '';
            var group_type = '';
            var source_city = '';
            var source_name = '';
            if (str == 1) {
                type = 'positive';
            } else if (str == 2) {
                type = 'negative';
            } else if (str == 3) {
                type = 'neutral';
            }

            if (sentiment_type == 'Location') {
                source_name = '';
                source_city = chart_value;
                var brands = $('#news-source-select option:selected');
//                console.log(brands);
                var selected = '';
                $(brands).each(function (index, brand) {
                    if (index == 0) {
                        selected = $(this).val();
                    } else {
                        selected += '%2C' + $(this).val();
                    }

                });
                source_name = selected;
            } else if (sentiment_type == 'Sources') {
                source_name = chart_value;
//                console.log(chart_value);
                source_city = '';
            } else if (sentiment_type == 'Sentiment') {
                source_name = '';
                var brands = $('#news-source-select option:selected');
                var selected = '';
                $(brands).each(function (index, brand) {
                    console.log(index);
                    if (index == 0) {
                        selected = $(this).val();
                    } else {
                        selected += '%2C' + $(this).val();
                    }

                });
                source_name = selected;
            }
//            console.log(chart_value);
//            console.log(source_name);

//            alert(selected);
//            console.log(type + ' - ' + source_name + ' - ' + sentiment_type);
            window.open(APP_URL + "/?search=" + text_search + "&search_city=" + source_city + "&search_source=" + source_name + "&daterange=" + date_range + "&sentiment_group=" + type);
//            var item = chart.getSelection()[0];
//            var str = data.getFormattedValue(item.row, item.column);
//            console.log(str);
//            var selectedItem = chart.getSelection()[0];
//            if (selectedItem) {
//                var value = data.getValue(selectedItem.row, selectedItem.column);
//                alert('The user selected ' + value);
//            }
        }


    }

</script>
</body>
</html>
