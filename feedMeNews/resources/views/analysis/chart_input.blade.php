<div class="content">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{--<a href="#">Brand</a>--}}
                <a class="navbar-brand" href="{{url('/')}}"><img width="130" style="margin-top: -8px;" height="60"
                                                                 src="{{asset('images/logo.png')}}"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            @if(Route::current()->uri() != 'mapview')
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    {!! Form::open(['url' => '/analysis', 'method' => 'get','id'=>'search_form','class'=>'navbar-form navbar-left']) !!}
                    <div class="input-group" style="margin-top:14px;">
                        <input id="search_input" name="search" type="text"
                               value="{{$current_search ? $current_search:''}}"
                               class="form-control" placeholder="#Keywords" style="width: 200px;margin-left: 14px">
                        <input id="search_city" name="search_city" type="hidden"
                               value="{{$current_city ? $current_city:''}}">
                        <input id="search_source" name="search_source" type="hidden"
                               value="{{$current_source ? $current_source:''}}">
                        <input id="search_group" name="type" type="hidden" value="{{$type_group}}">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><span
                                        class="glyphicon glyphicon-search"></span></button>
                        </span>
                    </div>

                    <input type="text" name="daterange" class="form-control"
                           style="margin-top:13px;margin-left:5px;width:185px"
                           value="{{$start_date}} - {{$current_date}}"/>
                    {!! Form::close() !!}
                    {{--<div>--}}
                    {{--<ul class="nav navbar-nav">--}}

                    {{--<li class="dropdown">--}}
                    {{--<a href="#" id="location_dropdown" class="dropdown-toggle" data-toggle="dropdown"--}}
                    {{--role="button"--}}
                    {{--aria-haspopup="true"--}}
                    {{--aria-expanded="false">{{$current_city ? $current_city:'Source Location'}}<span--}}
                    {{--class="caret"></span></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                    {{--@foreach($cities as $city)--}}
                    {{--<li><a class="dropdown-item search_city" href="#">{{$city[0]}}</a></li>--}}
                    {{--@endforeach--}}
                    {{--</ul>--}}
                    {{--</li>--}}
                    {{--</ul>--}}
                    {{--<div class="">--}}
                    {{--<label id="source_dropdown" for="sel1">{{$current_source ? $current_source:'Source Name'}}</label>--}}
                    {{--<select multiple class="select2" id="sel2">--}}
                    {{--@foreach($sources as $source)--}}
                    {{--<option class="search_source" href="#">{{$source[0]}}</option>--}}
                    {{--@endforeach--}}
                    {{--</select>--}}
                    {{--</div>--}}
                    <ul class="nav navbar-nav">
                        <li class="dropdown" style="margin-top: 21px;margin-left: -7px;">
                            <select id="news-source-select" multiple="multiple">
                                @foreach($sources as $source)
                                    {{--<li><a class="dropdown-item search_source" href="#">{{$source[0]}}</a></li>--}}
                                    @if(in_array($source[0], $selected_sources) || $selected_sources==[])
                                        <option value="{{$source[0]}}" selected="selected"
                                                class="search_source">{{$source[0]}}</option>
                                    @else
                                        <option value="{{$source[0]}}"
                                                class="search_source">{{$source[0]}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav">

                        <li class="dropdown">
                            <a href="#" id="group_dropdown" class="dropdown-toggle" data-toggle="dropdown"
                               role="button"
                               aria-haspopup="true"
                               aria-expanded="false">{{ $type_group ?  $type_group:'Chart'}}<span
                                        class="caret"></span></a>
                            <ul class="dropdown-menu">

                                <li><a class="dropdown-item type_group" href="#">By Sources</a></li>
                                <li><a class="dropdown-item type_group" href="#">By Location</a></li>
                                <li><a class="dropdown-item type_group" href="#">By Sentiment</a></li>
                            </ul>
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        @if (Route::has('login'))
                            @if (Auth::check())
                                <li><a href="{{ url('/home') }}">Account</a></li>
                            @else
                                <li class="active"><a href="{{ url('/analysis') }}">Analytics</a></li>
                                <li><a href="{{ url('/mapview') }}">MapView</a></li>
                                <li><a href="{{ url('/login') }}">Login</a></li>
                                <li><a href="{{ url('/register') }}">Register</a></li>
                            @endif
                        @endif
                    </ul>
                </div>
            @endif
        </div>
    </nav>
    @if(Route::current()->uri() != 'mapview')
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @foreach($keywords as $key)
                        <a style="margin-right: 2px;margin-bottom: 10px;margin-top: -10px;padding: 3px;font-size: 16px" type="button" href="#"
                           class="btn btn-sm btn-info keywords">#{{$key['_id']}}</a>&nbsp;&nbsp;&nbsp;&nbsp;
                    @endforeach
                </div>
            </div>
        </div>
    @endif

</div>