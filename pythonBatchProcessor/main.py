import happybase
import json
import re
import random
import time
import sentimod_v2 as s
from keywords import *
from pymongo import MongoClient
from rake_nltk import Rake

def keywordsFromText(text):
    r = Rake()
    text = unicode(text, 'utf-8')
    r.extract_keywords_from_text(text)
    return r.get_ranked_phrases()
	
connection = happybase.Connection('localhost')
connection.open()
print(connection.tables())
table = connection.table('newsitems')

client = MongoClient()
db=client.feedmenews

output = ''
i = 1
sentiments = ["positive", "negative", "neutral"]


for key, data in table.scan(row_prefix=b'2017-12-07', columns=[b'family1:description',b'family1:content',b'family2:sourceName',b'family2:location',b'family2:title',b'family2:categories']):
    date, url = key.split(b'|')
    #    print 'date: %s\n' %date
    #    print 'url: %s\n' %url
    #    print 'content: %s\n' %data['family1:content']
    #    print 'description: %s\n' %data['family1:description']
    if data[b'family2:title']:
        """
        starttime=time.time()
        contentDecoded=data[b'family1:content'].decode('utf-8')
        contentDecoded=re.sub(r"[€˜â€™�‘/œ\©ⓥã³¦å#”“†ð¡¼âãðÿ+ñ‡µœ¿‹¾§²¼å„гâ…]",'',contentDecoded)
        keywordsSetContent = getKeywords(contentDecoded)

        ellapsed = time.time()-starttime
        print ("Spent %f seconds on content decoding" % (ellapsed))
        
        titleDecoded=data[b'family2:title'].decode('utf-8')
        titleDecoded=re.sub(r"[€˜â€™�‘/œ\©”ⓥã³¦å#†ð¡¼âãðÿ+ñ‡µœ¿‹¾§²¼å„“гâ…]",'',titleDecoded)
        keywordsSetTitle = getKeywords(titleDecoded)

        categories=data[b'family2:categories'].decode('utf-8')
        categories=re.sub(r"[&]",',',categories)
        categories=re.sub(r"[.\[\]]",'',categories)

        categories=categories[1:-1]
        categories=categories.split(',')

        unionKeywords = keywordsSetContent.union(keywordsSetTitle)
        unionKeywords = unionKeywords.union(categories)

        unionKeywords = unionKeywords.difference({"'","@","+","â","€","“","”","/",
            "â©","‘","\\","–","[","]","…","—","|","l","â©","💙","’","","â€¦", "â€�"})

	"""

        starttime=time.time()
	
        title = data[b'family2:title'].decode('utf-8')
        sentimentV2,confidence = s.sentiment(title)

        ellapsed = time.time()-starttime
        print ("sentiment: %s confidence:%s time: %s title: %s\n" % (sentimentV2,confidence,ellapsed,title))
        
        output += "sent: %s conf: %f title: %s\n" % (sentimentV2,confidence,title)
        i=i+1
"""                                        
        result = db.articles.insert_one({
            "source": {
                "name": data['family2:sourceName'],
                "location": data['family2:location']
            },
            "title": data['family2:title'],
            "url": url,
            "category": "politics",
            "keywords": keywordsFromText(data['family1:content'])[:20],
            "sentiment": {
                "value": random.sample(sentiments,1)[0],
                "confidence": random.randint(1, 100)
            },
            "publishDate": date,
            "description": data['family1:description'].replace('\n','')
        })
"""

f = open('sentiment.txt','w')
f.write(output)
f.close()

