﻿import rake
import operator
# Install "NLTK" library (Windows, Linux) README file

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.tag import pos_tag
import string




#to check if we have only unique values in the list
def allUnique(x):
    seen = set()
    return not any(i in seen or seen.add(i) for i in x)


def getKeywords(text):
    # Each word has at least X characters - 1st number
    # Each phrase has at most X words -  2nd number
    # Each keyword appears in the text at least X times - the 3rd number
    rake_object = rake.Rake("SmartStoplist.txt", 2, 2, 2)
    #WE can input files in  different ways
    #sample_file = open("data/docs/fao_test/w2167e.txt", 'r')
    #text = sample_file.read()
    """

    text = "Bitcoin futures are expected to bring some stability to the wild cryptocurrency market, but not at the start." \
             "The Cboe is the first of three U.S. exchanges planning a futures product, and its contract, based on a single" \
             " coin unit, launches at 6 p.m. ET Sunday. Some trading firms and brokers plan to stand back and watch the" \
             " initial action, but others are wading in cautiously into a product that allows the first regulated two-way " \
             "trade where bitcoin can actually be shorted. The launch comes on the heels of a dramatic week of trading," \
             " where bitcoin rocketed from about $11,000 on Monday morning to above $19,000 Thursday " \
             "and back down to where it was near $15,000 Friday. The move by the U.S. exchanges could give legitimacy to the " \
             "cryptocraze, driven by investors worldwide, but not embraced by the biggest financial institutions. I think " \
             "the first hour will be really fun to watch because ... we're at the point where no one knows. " \
             "I think this is going to be one way or the other, you're going to get a tremendous amount of interest in " \
             "that first hour or you'll have people sitting around saying, 'What are we going to do? said JJ Kinahan, " \
             "chief strategist at TD Ameritrade. The Cboe leapfrogged the CME which starts trading its contract Dec. 18 " \
             "and the Nasdaq, which hopes to launch in the second quarter of next year. The U.S. exchanges have come under" \
             " criticism from the Futures Industry Association, a trade group for the world's biggest banks and financial " \
             "institutions, for rushing to join the cryptocurrency frenzy that many say has all the makings of a giant bubble." \
             " The FIA complained that the exchanges did not engage in enough dialogue with the clearing institutions for" \
             " a product that could be highly volatile and risky. Another issue for the futures market is that there is" \
             " no big cash market sanctioned by the financial establishment, so some of those institutions, like JPMorgan " \
             "and Citigroup, are not planning to initially provide clearing services, according to reports. The exchanges are" \
             " actively trying to head off some wild speculation by setting higher than normal margin requirements." \
             "The Cboe says it will require a minimum of 40 percent – up from its original 30 percent and about 10 times " \
             "the amount of margin required on a futures contract on something like the S&P 500. My gut says there's going" \
             " to be some wild volatility and swings in the first couple of days. You're going to see limit up, limit" \
             " down a lot, and then it'll probably settle down, but I really don't know, said Larry Tabb, founder and head" \
             " of research at Tabb Group."
             """

    keywords_score_all = rake_object.run(text)
    # print to just to demonstrate the
    # print("Keywords with score:", keywords_score_all, '*************')


    # We might have some limitation in DB and want to store keywords with score > X (here x = 2)
    keywords_score = [(a, b) for a, b in keywords_score_all if b > 2]


    # We probably  don't need score in our DB, so we keep only words
    keywords = [(i[0]) for i in keywords_score]
    # print("Keywords:", keywords, '***************')



    #################################################################
    # POLITICS - we are focusing on this category, so it's important to keep the Proper Names
    # DATA PREPARATION - the first step of the analysis
    # Removing stop words(English),  stemming,


    # EXTRACT PROPER NOUNS
    # NNP might not be the most frequent word, but they are important for the search
    tokens = word_tokenize(text)
    tagged = pos_tag(tokens)
    nouns = [word for word, pos in tagged if (pos == 'NNP')]
    unique_proper_words = set(nouns)



    # cleaning the set by removing months and weeks
    list1 = ['January','February', 'March', 'April','May','June','July','August','September','October',
             'November','December','Monday', 'Tuesday', 'Wednesday', 'Thursday',
             'Friday','Sunday', 'Saturday']
    result = (set(unique_proper_words) - set(list1))


    # merging 2 lists removing redundant values
    low_proper_noun = [element.lower() for element in result]
    mutual = (set(keywords) - set(low_proper_noun))


    # Combine the sets
    final_result = mutual.union(low_proper_noun)
    return {x.lstrip().rstrip() for x in final_result}



      
