import csv 

outputneg = outputpos = ''

with open('sentimentDataset10000.csv', newline='', encoding='utf8') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',')
    for row in spamreader:
        if row[1] == "0":
            outputneg += row[3].rstrip().lstrip() + "\n"
        if row[1] == "1":
            outputpos += row[3].rstrip().lstrip() + "\n"
f = open('smallDatasetNeg.csv', 'w')
f.write(outputneg)
f.close()

f = open('smallDatasetPos.csv','w')
f.write(outputpos)
f.close()
